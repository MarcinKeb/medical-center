﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Globalization;

namespace Przychodnia_zdrowia
{
    
    public partial class Modul_recepcji : Form
    {
        //ZMIENNE//
        
        string plec = " ";
        string ubezpieczenie = " ";
        string Polaczenie_z_baza = "datasource=localhost;port=3306;username=root;password=;Convert Zero Datetime=True;charset=utf8";
        int ilosc_textboxow = 0;
        public Modul_recepcji()
        {
            InitializeComponent();
            load_table_pacjenci();
            czysc_pacjent();
            Specjalizcja_combobox();
            Specjalizcja_lekarz_combobox();
            timer1.Start();
           
          
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }
      

       

        private void button1_Click_1(object sender, EventArgs e)
        {
            TabControl.SelectedIndex = 0;
            dateTimePicker1.Value = DateTime.Now; 
        }

        //ZAMKNIJ APLIKACJE//
        private void button9_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            TabControl.SelectedIndex = 1;
        }

        //PRZYCISK GŁÓWNY - DODAJ LEKARZA//
        private void button7_Click(object sender, EventArgs e)
        {
            
            TabControl.SelectedIndex = 2;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            TabControl.SelectedIndex = 3;
        }
        
        //POKAZ - TABELE PACJENCI//
        void load_table_pacjenci()
        {
            string ZapytaniePokazBazePacjenci = "select Id_pacjenta,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Ubezpieczenie,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.pacjenci where Aktywny=1;";

            MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazePacjenci = new MySqlCommand(ZapytaniePokazBazePacjenci, TPacjenci);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazePacjenci;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView3.DataSource = bSource;
                dataGridView4.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }  
        //PRZYCISK - DODAJ PACJENTA DO BAZY //
        private void button11_Click_1(object sender, EventArgs e)
        {
            if (((radioButton7.Checked == false) && (radioButton8.Checked == false)) || ((radioButton5.Checked == false) && (radioButton6.Checked == false)) || OknoNazwPacj.Text == "" || OknoImiePacj.Text == "" || maskedTextBox1.Text == "    -  -  " || OknoMiejUrodzPacj.Text == "" || maskedTextBox4.Text == "" ||  OknoMiastoPacj.Text == "" || maskedTextBox2.Text == "  -   " || OknoUlicaPacj.Text == "" || OknoNrDomuPacj.Text == "" || maskedTextBox3.Text == "   -   -   ")
            {
                MessageBox.Show("Uzupełnij wszystkie dane");

            }
            else
            {
                if (radioButton5.Checked)
                {
                    plec = radioButton5.Text;
                                   }
                if (radioButton6.Checked)
                {
                    plec = radioButton6.Text;
                }

                if (radioButton7.Checked)
                {
                    ubezpieczenie = radioButton7.Text;
                }
                if (radioButton8.Checked)
                {
                    ubezpieczenie = radioButton8.Text;
                }


                string ZapytanieDodajPacjenta = "insert into przychodnia.pacjenci (Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Ubezpieczenie,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon) values('" + this.OknoNazwPacj.Text + "','" + this.OknoImiePacj.Text + "','" + maskedTextBox1.Text + "','" + this.OknoMiejUrodzPacj.Text + "','" + plec + "','" + this.maskedTextBox4.Text + "','" + ubezpieczenie + "','" + this.OknoMiastoPacj.Text + "','" + this.maskedTextBox2.Text + "','" + this.OknoUlicaPacj.Text + "','" + this.OknoNrDomuPacj.Text + "','" + this.maskedTextBox3.Text + "') ;";

                MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand KwerendaDodajPacjenta = new MySqlCommand(ZapytanieDodajPacjenta, TPacjenci);
                MySqlDataReader CzytaniePacjent;

                try
                {
                    TPacjenci.Open();
                    CzytaniePacjent = KwerendaDodajPacjenta.ExecuteReader();
                    MessageBox.Show("Zapisano");

                    while (CzytaniePacjent.Read())
                    {
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                load_table_pacjenci();
            }
        }
        private void button15_Click(object sender, EventArgs e)
        {
            load_table_pacjenci();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                plec = radioButton5.Text;
            }
            if (radioButton6.Checked)
            {
                plec = radioButton6.Text;
            }
            if (radioButton7.Checked)
            {
                ubezpieczenie = radioButton7.Text;
            }
            if (radioButton8.Checked)
            {
                ubezpieczenie = radioButton8.Text;
            }
            string ZapytanieDodajPacjent = "update przychodnia.pacjenci set Id_pacjenta='" +this.Id_pacjenta.Text + "',Nazwisko='" + this.OknoNazwPacj.Text + "',Imie='" + this.OknoImiePacj.Text + "',Data_urodzenia='" + this.maskedTextBox1.Text + "',Miejsce_urodzenia='" + this.OknoMiejUrodzPacj.Text + "',Plec='" + plec + "',Pesel='" + this.maskedTextBox4.Text + "',Ubezpieczenie='" + ubezpieczenie + "',Miasto='" + this.OknoMiastoPacj.Text + "',Kod_pocztowy='" + this.maskedTextBox2.Text + "',Ulica='" + this.OknoUlicaPacj.Text + "',Nr_domu='" + this.OknoNrDomuPacj.Text + "',Telefon='" + this.maskedTextBox3.Text + "' where Id_pacjenta='" + this.Id_pacjenta.Text + "' ;";

            MySqlConnection TPacjent = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajPacjent = new MySqlCommand(ZapytanieDodajPacjent, TPacjent);
            MySqlDataReader CzytaniePacjent;

            try
            {
                TPacjent.Open();
                CzytaniePacjent = KwerendaDodajPacjent.ExecuteReader();
                MessageBox.Show("Zaktualizowano");

                while (CzytaniePacjent.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_pacjenci();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                plec = radioButton5.Text;
            }
            if (radioButton6.Checked)
            {
                plec = radioButton6.Text;
            }
            if (radioButton7.Checked)
            {
                ubezpieczenie = radioButton7.Text;
            }
            if (radioButton8.Checked)
            {
                ubezpieczenie = radioButton8.Text;
            }
            string ZapytanieDodajPacjent = "update przychodnia.pacjenci set Id_pacjenta='" + this.Id_pacjenta.Text + "',Nazwisko='" + this.OknoNazwPacj.Text + "',Imie='" + this.OknoImiePacj.Text + "',Data_urodzenia='" + this.maskedTextBox1.Text + "',Miejsce_urodzenia='" + this.OknoMiejUrodzPacj.Text + "',Plec='" + plec + "',Pesel='" + this.maskedTextBox4.Text + "',Ubezpieczenie='" + ubezpieczenie + "',Miasto='" + this.OknoMiastoPacj.Text + "',Kod_pocztowy='" + this.maskedTextBox2.Text + "',Ulica='" + this.OknoUlicaPacj.Text + "',Nr_domu='" + this.OknoNrDomuPacj.Text + "',Telefon='" + this.maskedTextBox3.Text + "',Aktywny='" + 0 + "' where Id_pacjenta='" + this.Id_pacjenta.Text + "' ;";

            MySqlConnection TPacjent = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajPacjent = new MySqlCommand(ZapytanieDodajPacjent, TPacjent);
            MySqlDataReader CzytaniePacjent;

            try
            {
                TPacjent.Open();
                CzytaniePacjent = KwerendaDodajPacjent.ExecuteReader();
                MessageBox.Show("Usunięto");

                while (CzytaniePacjent.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_pacjenci();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            czysc_pacjent();
        }
        //Funkcja czyszcząca pola - pacjent//
        void czysc_pacjent()
        {
            this.OknoNazwPacj.Clear();
            this.OknoImiePacj.Clear();
            this.maskedTextBox1.Clear();
            this.OknoMiejUrodzPacj.Clear();
            radioButton5.Checked = false;
            radioButton6.Checked = false;
            radioButton7.Checked = false;
            radioButton8.Checked = false;
            this.maskedTextBox4.Clear();
            this.OknoMiastoPacj.Clear();
            this.maskedTextBox2.Clear();
            this.OknoUlicaPacj.Clear();
            this.OknoNrDomuPacj.Clear();
            this.maskedTextBox3.Clear();
        }

        private void button60_Click(object sender, EventArgs e)
        {
            if (textBox77.Text == "" || textBox78.Text == "" || comboBox1.Text == "" || comboBox2.Text == "" || comboBox3.Text == "")
            {
                MessageBox.Show("Uzupełnij dane do rejetracji");
                
            }
            else
            {
                if ((dateTimePicker1.Value >= DateTime.Today))
                {
                   
                    for (int i = 0; i < textboxy.Length; i++)
                    {
                        if ((textboxy3[i].Text == textBox79.Text))
                        {
                            MessageBox.Show("Brak możliwości rejetracji tej samej osoby drugi raz");
                            break;
                        }
                        else
                        {
                            if ((comboBox3.Text) == (textboxy[i].Text))
                            {
                                if ((textboxy2[i].Text) != "")
                                {
                                    MessageBox.Show("Ta godzina jest już zajęta");
                                    break;
                                }
                                if ((textboxy2[i].Text) == "")
                                {
                                    string ZapytanieDodajDoRejestracji = "insert into przychodnia.info_wizyta (Data_wizyty,Godzina_wizyty,Id_lekarza,Id_pacjenta) values('" + this.dateTimePicker1.Text + "','" + comboBox3.Text + "','" + textBox80.Text + "','" + textBox79.Text + "') ;";

                                    MySqlConnection TLekarz = new MySqlConnection(Polaczenie_z_baza);
                                    MySqlCommand KwerendaDodajLekarza = new MySqlCommand(ZapytanieDodajDoRejestracji, TLekarz);
                                    MySqlDataReader CzytanieLekarz;

                                    try
                                    {
                                        TLekarz.Open();
                                        CzytanieLekarz = KwerendaDodajLekarza.ExecuteReader();
                                        MessageBox.Show("Umówiono");

                                        while (CzytanieLekarz.Read())
                                        {
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message);
                                    }
                                    DateTime dt = new DateTime(2008, 03, 09);
                                    DateTime temp2 = dt;
                                    DateTime temp = dateTimePicker1.Value.Date;

                                    dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
                                    dateTimePicker1.Value = temp2;
                                    dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
                                    dateTimePicker1.Value = temp;
                                    dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
                                    break;
                                }
                            }
                        }
                    }
                }
                else{
                    MessageBox.Show("Nie można umawiać wizyt w przeszłość! ");
                }
            }
        }

        void Specjalizcja_combobox()
        {
            string Zapytanie = "select DISTINCT Specjalizacja from przychodnia.lekarze where Aktywny=1 ;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazSpecjalizacje = new MySqlCommand(Zapytanie, TLekarze);
            MySqlDataReader CzytanieLekarz;
            

            try
            {
                TLekarze.Open();
                CzytanieLekarz = KwerendaPokazSpecjalizacje.ExecuteReader();

                while (CzytanieLekarz.Read())
                {
                    string Specj = CzytanieLekarz.GetString("Specjalizacja");
                    comboBox1.Items.Add(Specj);                                                        
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Specjalizcja_lekarz_combobox()
        {
            this.comboBox2.Items.Clear();
            comboBox2.ResetText();
            string Zapytanie = "select * from przychodnia.lekarze where Specjalizacja='" + comboBox1.Text + "' ;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazSpecjalizacje = new MySqlCommand(Zapytanie, TLekarze);
            MySqlDataReader CzytanieLekarz;
            
            try
            {
                TLekarze.Open();
                CzytanieLekarz = KwerendaPokazSpecjalizacje.ExecuteReader();

                while (CzytanieLekarz.Read())
                {
                    string Specj = CzytanieLekarz.GetString("Nazwisko");
                    comboBox2.Items.Add(Specj);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
         flowLayoutPanel1.Controls.Clear();
         flowLayoutPanel2.Controls.Clear();
         flowLayoutPanel3.Controls.Clear();
         flowLayoutPanel4.Controls.Clear();
         Specjalizcja_lekarz_combobox();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime Czas = DateTime.Now;
           
            labTime.Text = Czas.ToString();
            labTime2.Text = Czas.ToString();
        }


        private void OknoRejSzukaj_TextChanged(object sender, EventArgs e)
        {
           
            string ZapytaniePokazBazePacjenci = "select Id_pacjenta,Nazwisko,Imie,Pesel,Miasto,Ubezpieczenie from przychodnia.pacjenci where Aktywny=1 Nazwisko like '" + this.OknoRejSzukaj.Text + "%" +  "' ;";

            MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazePacjenci = new MySqlCommand(ZapytaniePokazBazePacjenci, TPacjenci);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazePacjenci;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView4.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
           
        {
            int id;
            id = Convert.ToInt32(dataGridView4.Rows[e.RowIndex].Cells["Id_pacjenta"].Value.ToString());
            string ZapytaniePokazBazePacjenci = "select Id_pacjenta,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Ubezpieczenie,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.pacjenci where Aktywny=1 and Id_pacjenta=" + id + "";

            MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazePacjenci = new MySqlCommand(ZapytaniePokazBazePacjenci, TPacjenci);
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(KwerendaPodazBazePacjenci);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                textBox77.Text = dr["Nazwisko"].ToString();
                textBox78.Text = dr["Imie"].ToString();
                textBox79.Text = dr["Id_pacjenta"].ToString();
            }
        }
               
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            flowLayoutPanel2.Controls.Clear();
            flowLayoutPanel3.Controls.Clear();
            flowLayoutPanel4.Controls.Clear();
            this.textBox80.Clear();
            textBox80.ResetText();
          

            string Zapytanie = "select * from przychodnia.lekarze where Nazwisko='" + comboBox2.Text + "' ;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);            
            MySqlCommand KwerendaPokazSpecjalizacje = new MySqlCommand(Zapytanie, TLekarze);           
            MySqlDataReader CzytanieLekarz;            

            label51.Text = comboBox2.SelectedItem.ToString();
            try
            {
                TLekarze.Open();
                CzytanieLekarz = KwerendaPokazSpecjalizacje.ExecuteReader();

                while (CzytanieLekarz.Read())
                {
                    string Id = CzytanieLekarz.GetString("Id_lekarza");
                    textBox80.Text = Id;                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            listView1.Items.Clear();
            listView1.View = View.Details;
            MySqlConnection TDyzury = new MySqlConnection(Polaczenie_z_baza);
            string ZapytanieDyzury = "select dzien,godzina_od,godzina_do,gabinet from przychodnia.dyzury where Id_lekarza='" + textBox80.Text + "' ;";
            MySqlCommand KwerendaPokazDyzury = new MySqlCommand(ZapytanieDyzury, TDyzury);
            MySqlDataAdapter sda = new MySqlDataAdapter();
            sda.SelectCommand = KwerendaPokazDyzury;
            DataTable dt = new DataTable();
            sda.Fill(dt);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dane = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dane["dzien"].ToString());
                listitem.SubItems.Add(dane["godzina_od"].ToString());
                listitem.SubItems.Add(dane["godzina_do"].ToString());
                listitem.SubItems.Add(dane["gabinet"].ToString());
                listView1.Items.Add(listitem);
            }

            DateTime dw = new DateTime(2008, 03, 09);
            DateTime temp2 = dw;
            DateTime temp = dateTimePicker1.Value.Date;

            dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
            dateTimePicker1.Value = temp2;
            dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
            dateTimePicker1.Value = temp;
            dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);          
      
        }

        public System.Windows.Forms.TextBox[] textboxy;
        public System.Windows.Forms.TextBox[] textboxy2;
        public System.Windows.Forms.TextBox[] textboxy3;
        public System.Windows.Forms.TextBox[] textboxy4;

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            
            flowLayoutPanel1.Controls.Clear();
            flowLayoutPanel2.Controls.Clear();
            flowLayoutPanel3.Controls.Clear();
            flowLayoutPanel4.Controls.Clear();
            comboBox3.Items.Clear();
            
            
            CultureInfo polski = new CultureInfo("pl-PL", false);
            DateTime dateString = dateTimePicker1.Value;
            string dzienTygodnia = polski.DateTimeFormat.DayNames[(int)dateString.DayOfWeek];

            label49.Text = dzienTygodnia;
            string dzien_tyg;
            int poczatek_godzina = 0;
            int koniec_godzina = 0;
            int ilosc_wierszy = listView1.Items.Count;

            for (int wiersz = 0; wiersz < ilosc_wierszy; wiersz++)
            {
                dzien_tyg = listView1.Items[wiersz].SubItems[0].Text;

                if (dzien_tyg == label49.Text)
                {
                    poczatek_godzina = Convert.ToInt32(listView1.Items[wiersz].SubItems[1].Text.Remove(2));
                    koniec_godzina = Convert.ToInt32(listView1.Items[wiersz].SubItems[2].Text.Remove(2));
                }

            }
            int minuta = 0;
            int licznik = 0;

            ilosc_textboxow = (koniec_godzina - poczatek_godzina) * 3;      

            textboxy = new System.Windows.Forms.TextBox[ilosc_textboxow];
            for (int i = 0; i < textboxy.Length; i++)
            {
                textboxy[i] = new TextBox();
                textboxy[i].Location = new System.Drawing.Point(0, 0);
                textboxy[i].Name = "textbox" + i.ToString();
                textboxy[i].Size = new System.Drawing.Size(40, 20);
                textboxy[i].BackColor = System.Drawing.Color.Khaki;
                textboxy[i].BorderStyle = BorderStyle.None;
                textboxy[i].TabIndex = i;
                textboxy[i].Enabled = false;
                    textboxy[i].Text = poczatek_godzina + ":" + minuta + "0";
                    comboBox3.Items.Add(textboxy[i].Text);

                    licznik++; minuta += 2;
                    if (minuta > 4) { minuta = 0; };
                    if (licznik == 3 || licznik == 6 || licznik == 9 || licznik == 12 || licznik == 15
                    || licznik == 18 || licznik == 21 || licznik == 24 || licznik == 27 || licznik == 30) { poczatek_godzina++; };
                flowLayoutPanel1.Controls.Add(textboxy[i]);
            }
            textboxy2 = new System.Windows.Forms.TextBox[ilosc_textboxow];
            for (int i = 0; i < textboxy2.Length; i++)
            {
                textboxy2[i] = new TextBox();
                textboxy2[i].Location = new System.Drawing.Point(0, 0);
                textboxy2[i].Name = "textboxy" + i.ToString();
                textboxy2[i].Size = new System.Drawing.Size(65, 20);
                textboxy2[i].BackColor = System.Drawing.Color.GreenYellow;
                textboxy2[i].BorderStyle = BorderStyle.None;
                textboxy2[i].TabIndex = i;
           
                textboxy2[i].Enabled = false;
                flowLayoutPanel2.Controls.Add(textboxy2[i]);
            }
            textboxy3 = new System.Windows.Forms.TextBox[ilosc_textboxow];
            for (int i = 0; i < textboxy2.Length; i++)
            {
                textboxy3[i] = new TextBox();
                textboxy3[i].Location = new System.Drawing.Point(0, 0);
                textboxy3[i].Name = "textboxy" + i.ToString();
                textboxy3[i].Size = new System.Drawing.Size(65, 20);
                textboxy3[i].BackColor = System.Drawing.Color.GreenYellow;
                textboxy3[i].BorderStyle = BorderStyle.None;
                textboxy3[i].TabIndex = i;

                textboxy3[i].Enabled = false;
                flowLayoutPanel3.Controls.Add(textboxy3[i]);
            }
            textboxy4 = new System.Windows.Forms.TextBox[ilosc_textboxow];
            for (int i = 0; i < textboxy4.Length; i++)
            {
                textboxy4[i] = new TextBox();
                textboxy4[i].Location = new System.Drawing.Point(0, 0);
                textboxy4[i].Name = "textboxy4" + i.ToString();
                textboxy4[i].Size = new System.Drawing.Size(65, 20);
                textboxy4[i].BackColor = System.Drawing.Color.GreenYellow;
                textboxy4[i].BorderStyle = BorderStyle.None;
                textboxy4[i].TabIndex = i;

                textboxy4[i].Enabled = false;
                 flowLayoutPanel4.Controls.Add(textboxy4[i]);
            }
            MySqlConnection TInfo_wizyta = new MySqlConnection(Polaczenie_z_baza);

            string ZapytanieInfo_wizyta = "select * from przychodnia.pacjenci INNER JOIN przychodnia.info_wizyta ON przychodnia.pacjenci.Id_pacjenta=przychodnia.info_wizyta.Id_pacjenta  ;";           
            MySqlCommand KwerendaPokazInfo_wizyte = new MySqlCommand(ZapytanieInfo_wizyta, TInfo_wizyta);
            MySqlDataReader CzytanieInfo_wizyta;

            try
            {
                TInfo_wizyta.Open();
                CzytanieInfo_wizyta = KwerendaPokazInfo_wizyte.ExecuteReader();
                while (CzytanieInfo_wizyta.Read())
                    {
                       if (CzytanieInfo_wizyta.GetDateTime("Data_wizyty") == (Convert.ToDateTime(this.dateTimePicker1.Text)))
                        {
                            for (int i = 0; i < textboxy.Length; i++)
                            {
                                if (CzytanieInfo_wizyta.GetString("Godzina_wizyty") == (textboxy[i].Text))
                                {
                                    string nazwisko = CzytanieInfo_wizyta.GetString("Nazwisko");
                                    textboxy2[i].Text = nazwisko;
                                    textboxy2[i].BackColor = System.Drawing.Color.Salmon;
                                    string id = CzytanieInfo_wizyta.GetString("Id_pacjenta");
                                    textboxy3[i].Text = id; ;
                                    textboxy3[i].BackColor = System.Drawing.Color.Salmon;
                                    string imie = CzytanieInfo_wizyta.GetString("Imie");
                                    textboxy4[i].Text = imie;
                                    textboxy4[i].BackColor = System.Drawing.Color.Salmon;
                                }
                            }
                                                   
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void button20_Click(object sender, EventArgs e)
        {
            if (textBox77.Text == "" || textBox78.Text == "" || comboBox1.Text == "" || comboBox2.Text == "" || comboBox3.Text == "")
            {
                MessageBox.Show("Uzupełnij dane jeśli chcesz usunąć wizytę");
            }
            else
            {
                if (dateTimePicker1.Value < DateTime.Now)
                {
                    MessageBox.Show("Nie można usuwać wizyt, które się odbyły");
                    goto Idz;
                }
                else
                {
                    for (int i = 0; i < textboxy2.Length; i++)
                    {
                        if ((textboxy2[i].Text == textBox77.Text))
                        {

                            string Id_info = "";
                            string Zapytanie = "select * from przychodnia.info_wizyta where Data_wizyty='" + this.dateTimePicker1.Text + "'and Godzina_wizyty='" + comboBox3.Text + "'and Id_lekarza='" + textBox80.Text + "'and Id_pacjenta='" + textBox79.Text + "' ;";

                            MySqlConnection TLekarz = new MySqlConnection(Polaczenie_z_baza);
                            MySqlCommand KwerendaDodajLekarza = new MySqlCommand(Zapytanie, TLekarz);
                            MySqlDataReader CzytanieLekarz;

                            try
                            {
                                TLekarz.Open();
                                CzytanieLekarz = KwerendaDodajLekarza.ExecuteReader();


                                while (CzytanieLekarz.Read())
                                {
                                    Id_info = CzytanieLekarz.GetString("Id_info");
                                    if (Id_info != "")
                                    {
                                        string ZapytanieUsunPacjenta = "delete from przychodnia.info_wizyta where Id_info='" + Id_info + "';";

                                        MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
                                        MySqlCommand KwerendaUsunPacjenta = new MySqlCommand(ZapytanieUsunPacjenta, TPacjenci);
                                        MySqlDataReader CzytaniePacjenta;

                                        try
                                        {
                                            TPacjenci.Open();
                                            CzytaniePacjenta = KwerendaUsunPacjenta.ExecuteReader();


                                            while (CzytaniePacjenta.Read())
                                            {

                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show(ex.Message);
                                        }
                                        DateTime dt = new DateTime(2008, 03, 09);
                                        DateTime temp2 = dt;
                                        DateTime temp = dateTimePicker1.Value.Date;

                                        dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
                                        dateTimePicker1.Value = temp2;
                                        dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
                                        dateTimePicker1.Value = temp;
                                        dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
                                        goto Koncz;
                                    }

                                }

                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
                
                MessageBox.Show("Taka wizyta nie istnieje");
                goto Idz;
            Koncz:
                MessageBox.Show("Usunięto wizytę");
        Idz:
        label53.Text = "";
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            this.Hide();
            Logowanie FormaLogowanie = new Logowanie();
            FormaLogowanie.Show();
            MessageBox.Show("Pomyślnie wylogowano");
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;
            id = Convert.ToInt32(dataGridView3.Rows[e.RowIndex].Cells["Id_pacjenta"].Value.ToString());
            Id_pacjenta.Text = Convert.ToString(id);
            string ZapytaniePokazBazePacjenci = "select Id_pacjenta,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Ubezpieczenie,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.pacjenci where Aktywny=1 and Id_pacjenta=" + id + "";

            MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazePacjenci = new MySqlCommand(ZapytaniePokazBazePacjenci, TPacjenci);
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(KwerendaPodazBazePacjenci);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                OknoNazwPacj.Text = dr["Nazwisko"].ToString();
                OknoImiePacj.Text = dr["Imie"].ToString();
                maskedTextBox1.Text = dr["Data_urodzenia"].ToString();
                OknoMiejUrodzPacj.Text = dr["Miejsce_urodzenia"].ToString();
                maskedTextBox4.Text = dr["Pesel"].ToString();
                OknoMiastoPacj.Text = dr["Miasto"].ToString();
                maskedTextBox2.Text = dr["Kod_pocztowy"].ToString();
                OknoUlicaPacj.Text = dr["Ulica"].ToString();
                OknoNrDomuPacj.Text = dr["Nr_domu"].ToString();
                maskedTextBox3.Text = dr["Telefon"].ToString();
                if ((dr["Plec"].ToString()) == "Mezczyzna") { radioButton5.Checked = true; };
                if ((dr["Plec"].ToString()) == "Kobieta") { radioButton6.Checked = true; };
                if ((dr["Ubezpieczenie"].ToString()) == "Tak") { radioButton7.Checked = true; };
                if ((dr["Ubezpieczenie"].ToString()) == "Nie") { radioButton8.Checked = true; };
            }
        }

        private void OknoNazwPacj_TextChanged(object sender, EventArgs e)
        {
            string ZapytaniePokazBazePacjenci = "select Id_pacjenta,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Ubezpieczenie,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.pacjenci where Aktywny=1 and Nazwisko like '" + this.OknoNazwPacj.Text + "%" + "' ;";

            MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazePacjenci = new MySqlCommand(ZapytaniePokazBazePacjenci, TPacjenci);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazePacjenci;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();
                bSource.DataSource = dbdataset;
                dataGridView3.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked == true)
            {
                pictureBox1.Visible = true;
                pictureBox2.Visible = false;
            }
           
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton6.Checked == true)
            {
                pictureBox2.Visible = true;
                pictureBox1.Visible = false;
            }
        }

        private void Modul_recepcji_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Czy napewno chcesz zamknąć aplikację?", "Zamykanie aplikacji", MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                {
                     Environment.Exit(0);
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void Modul_recepcji_Load(object sender, EventArgs e)
        {            
            dateTimePicker1.Value = DateTime.Now;
        }

        private void button21_Click(object sender, EventArgs e)
        {
            Dodaj_dyzur FormaDodaj_dyzur = new Dodaj_dyzur();
            FormaDodaj_dyzur.Show();
        }
     }
}
