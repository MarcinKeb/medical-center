﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Przychodnia_zdrowia
{
    public partial class Logowanie : Form
    {
        string Polaczenie_z_baza = "datasource=localhost;port=3306;username=root;password=;charset=utf8";
        public Logowanie()
        {
           InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                MySqlConnection TRecepcja = new MySqlConnection(Polaczenie_z_baza);
                MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
                MySqlConnection TAdministracja = new MySqlConnection(Polaczenie_z_baza);

                MySqlCommand ZapytanieRecepcja = new MySqlCommand("select * from przychodnia.recepcja where Pesel='" + this.OknoLog.Text + "'and Nazwisko='" + this.OknoHas.Text + "'and AKtywny=1 ;", TRecepcja);
                MySqlCommand ZapytanieLekarze = new MySqlCommand("select * from przychodnia.lekarze where Pesel='" + this.OknoLog.Text + "'and Nazwisko='" + this.OknoHas.Text + "'and AKtywny=1;", TLekarze);
                MySqlCommand ZapytanieAdministracja = new MySqlCommand("select * from przychodnia.administracja where Login='" + this.OknoLog.Text + "'and Haslo='" + this.OknoHas.Text + "' ;", TAdministracja);

                MySqlDataReader CzytanieRecepcja;
                MySqlDataReader CzytanieLekarze;
                MySqlDataReader CzytanieAdministracja;

                TRecepcja.Open();
                TLekarze.Open();
                TAdministracja.Open();

                CzytanieRecepcja = ZapytanieRecepcja.ExecuteReader();
                CzytanieLekarze = ZapytanieLekarze.ExecuteReader();
                CzytanieAdministracja = ZapytanieAdministracja.ExecuteReader();

                int Recepcja = 0;
                int Lekarz = 0;
                int Administracja = 0;


                while (CzytanieRecepcja.Read())
                {
                    Recepcja += 1;
                }
                while (CzytanieLekarze.Read())
                {
                    Lekarz += 1;
                }
                while (CzytanieAdministracja.Read())
                {
                    Administracja += 1;
                }
                if ((Recepcja == 1) || (Lekarz == 1) || (Administracja == 1))
                {
                    if (Recepcja == 1)
                    {
                        MessageBox.Show("Zalogowano jako recepcjonistka");
                        this.Hide();
                        Modul_recepcji FormaRejestracji = new Modul_recepcji();
                        FormaRejestracji.label4.Text = OknoHas.Text;
                        FormaRejestracji.ShowDialog();

                    }
                    if (Lekarz == 1)
                    {
                        MessageBox.Show("Zalogowano jako lekarz");
                        this.Hide();
                        Modul_lekarz FormaLekarz = new Modul_lekarz();
                        FormaLekarz.label1.Text = OknoHas.Text;
                        string Id = CzytanieLekarze.GetString("Id_lekarza");
                        FormaLekarz.textBox2.Text = Id;
                        FormaLekarz.ShowDialog();
                    }
                    if (Administracja == 1)
                    {
                        MessageBox.Show("Zalogowano jako administrator");
                        this.Hide();
                        Administracja FormaAdministracja = new Administracja();
                        FormaAdministracja.ShowDialog();
                    }
                }
                else if ((Recepcja > 1) || (Lekarz > 1) || (Administracja > 1))
                {
                    MessageBox.Show("Powtarzające się rekordy");
                }
                else
                    MessageBox.Show("Nie prawidłowe login lub hasło");

                TRecepcja.Close();
                TLekarze.Close();
                TAdministracja.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

      
       
    }
}
