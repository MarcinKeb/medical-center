﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Globalization;

namespace Przychodnia_zdrowia
{
    public partial class Dodaj_dyzur : Form
    {
        string Polaczenie_z_baza = "datasource=localhost;port=3306;username=root;password=;Convert Zero Datetime=True;charset=utf8";
        public Dodaj_dyzur()
        {
            InitializeComponent();
            Specjalizcja_combobox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Zapytanie = "insert into przychodnia.dyzury (Id_lekarza,dzien,godzina_od,godzina_do,gabinet) values('" + this.textBox3.Text + "','" + comboBox2.Text + "','" + maskedTextBox1.Text + "','" + maskedTextBox2.Text + "','" + textBox1.Text +"') ;";

            MySqlConnection TLekarz = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajLekarza = new MySqlCommand(Zapytanie, TLekarz);
            MySqlDataReader CzytanieLekarz;

            try
            {
                TLekarz.Open();
                CzytanieLekarz = KwerendaDodajLekarza.ExecuteReader();
                MessageBox.Show("Zapisano");

                while (CzytanieLekarz.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void Specjalizcja_combobox()
        {
            string Zapytanie = "select DISTINCT Nazwisko, Imie from przychodnia.lekarze  ;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazSpecjalizacje = new MySqlCommand(Zapytanie, TLekarze);
            MySqlDataReader CzytanieLekarz;


            try
            {
                TLekarze.Open();
                CzytanieLekarz = KwerendaPokazSpecjalizacje.ExecuteReader();

                while (CzytanieLekarz.Read())
                {
                    string Nazwisko = CzytanieLekarz.GetString("Nazwisko");
                    comboBox1.Items.Add(Nazwisko);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Zapytanie = "select * from przychodnia.lekarze where Nazwisko='" + comboBox1.Text + "' ;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazSpecjalizacje = new MySqlCommand(Zapytanie, TLekarze);
            MySqlDataReader CzytanieLekarz;

           
            try
            {
                TLekarze.Open();
                CzytanieLekarz = KwerendaPokazSpecjalizacje.ExecuteReader();

                while (CzytanieLekarz.Read())
                {
                    string Id = CzytanieLekarz.GetString("Id_lekarza");
                    textBox3.Text = Id;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string Id_dyzuru = " ";
            string Zapytanie = "select * from przychodnia.dyzury where Id_lekarza='" + this.textBox3.Text + "'and dzien='" + comboBox2.Text + "'and godzina_od='" + maskedTextBox1.Text + "'and godzina_do='" + maskedTextBox2.Text + "' ;";

            MySqlConnection TLekarz = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajLekarza = new MySqlCommand(Zapytanie, TLekarz);
            MySqlDataReader CzytanieLekarz;

            try
            {
                TLekarz.Open();
                CzytanieLekarz = KwerendaDodajLekarza.ExecuteReader();
                MessageBox.Show("Usunięto dyzur");

                while (CzytanieLekarz.Read())
                {
                    Id_dyzuru = CzytanieLekarz.GetString("Id_dyzuru");

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            string ZapytanieUsunDyzur = "delete from przychodnia.dyzury where Id_dyzuru='" + Id_dyzuru + "';";

            MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaUsunPacjenta = new MySqlCommand(ZapytanieUsunDyzur, TPacjenci);
            MySqlDataReader CzytaniePacjenta;

            try
            {
                TPacjenci.Open();
                CzytaniePacjenta = KwerendaUsunPacjenta.ExecuteReader();
                MessageBox.Show("Usunięto");

                while (CzytaniePacjenta.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
