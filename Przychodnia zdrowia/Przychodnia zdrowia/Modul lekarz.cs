﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.Diagnostics;


using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Przychodnia_zdrowia
{
    public partial class Modul_lekarz : Form
    {
        string Polaczenie_z_baza = "datasource=localhost;port=3306;username=root;password=;Convert Zero Datetime=True;charset=utf8";
        int ilosc_textboxow = 0;

        public System.Windows.Forms.TextBox[] textboxy;
        public System.Windows.Forms.TextBox[] textboxy2;
        public System.Windows.Forms.TextBox[] textboxy3;
        public System.Windows.Forms.TextBox[] textboxy4;
        

        public Modul_lekarz()
        {
            InitializeComponent();
            load_table_lekarstwa();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }
        void pokaz_pacjenta(string id)
        {
            label15.Visible = false; label16.Visible = false; 
            button4.Enabled = false; button3.Enabled = false; label11.Text = "";
            comboBox1.Enabled = false; comboBox1.Text = null; comboBox2.Enabled = false; comboBox2.Text = null;
            comboBox3.Enabled = false; comboBox3.Text = null; comboBox4.Enabled = false; comboBox4.Text = null;
            comboBox5.Enabled = false; comboBox5.Text = null; comboBox6.Text = null;
            dateTimePicker2.Enabled = false; dateTimePicker3.Enabled = false;
            checkBox1.Checked = false; checkBox2.Checked = false;


            string Zapytanie = "select * from przychodnia.pacjenci where Id_pacjenta='" + id + "';";

            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazSpecjalizacje = new MySqlCommand(Zapytanie, TLekarze);
            MySqlDataReader CzytanieLekarz;

            try
            {
                TLekarze.Open();
                CzytanieLekarz = KwerendaPokazSpecjalizacje.ExecuteReader();

                while (CzytanieLekarz.Read())
                {
                    label11.Text = CzytanieLekarz.GetString("Id_pacjenta");
                    string Nazwisko = CzytanieLekarz.GetString("Nazwisko");
                    label6.Text = Nazwisko;
                    string Imie = CzytanieLekarz.GetString("Imie");
                    label7.Text = Imie;
                    string Pesel = CzytanieLekarz.GetString("Pesel");
                    label10.Text = Pesel;
                    string Ubezpieczenie = CzytanieLekarz.GetString("Ubezpieczenie");
                    label14.Text = Ubezpieczenie;
                    if (label14.Text == "Nie")
                    {
                        label15.Visible = true;
                        label16.Visible = true;
                        button8.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Karta_pacjenta();
        }

        private void Modul_lekarz_Load(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            flowLayoutPanel2.Controls.Clear();

            string Zapytanie = "select * from przychodnia.lekarze where Id_lekarza='" + textBox2.Text + "' ;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazSpecjalizacje = new MySqlCommand(Zapytanie, TLekarze);
            MySqlDataReader CzytanieLekarz;

            try
            {
                TLekarze.Open();
                CzytanieLekarz = KwerendaPokazSpecjalizacje.ExecuteReader();

                while (CzytanieLekarz.Read())
                {
                    string Id = CzytanieLekarz.GetString("Id_lekarza");
                    textBox2.Text = Id;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            listView1.Items.Clear();
            listView1.View = View.Details;
            MySqlConnection TDyzury = new MySqlConnection(Polaczenie_z_baza);
            string ZapytanieDyzury = "select dzien,godzina_od,godzina_do,gabinet from przychodnia.dyzury where Id_lekarza='" + textBox2.Text + "' ;";
            MySqlCommand KwerendaPokazDyzury = new MySqlCommand(ZapytanieDyzury, TDyzury);
            MySqlDataAdapter sda = new MySqlDataAdapter();
            sda.SelectCommand = KwerendaPokazDyzury;
            DataTable dt = new DataTable();
            sda.Fill(dt);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dane = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dane["dzien"].ToString());
                listitem.SubItems.Add(dane["godzina_od"].ToString());
                listitem.SubItems.Add(dane["godzina_do"].ToString());
                listitem.SubItems.Add(dane["gabinet"].ToString());
                listView1.Items.Add(listitem);
            }
            dzisiejsze_wizyty();
        }


        void dzisiejsze_wizyty()
        {

            CultureInfo polski = new CultureInfo("pl-PL", false);

            DateTime dateString = DateTime.Now;
            string dzienTygodnia = polski.DateTimeFormat.DayNames[(int)dateString.DayOfWeek];

            label49.Text = dzienTygodnia;
            string dzien_tyg;
            int poczatek_godzina = 0;
            int koniec_godzina = 0;
            int ilosc_wierszy = listView1.Items.Count;

            for (int wiersz = 0; wiersz < ilosc_wierszy; wiersz++)
            {
                dzien_tyg = listView1.Items[wiersz].SubItems[0].Text;

                if (dzien_tyg == label49.Text)
                {
                    poczatek_godzina = Convert.ToInt32(listView1.Items[wiersz].SubItems[1].Text.Remove(2));
                    koniec_godzina = Convert.ToInt32(listView1.Items[wiersz].SubItems[2].Text.Remove(2));
                }

            }
            int minuta = 0;
            int licznik = 0;

            ilosc_textboxow = (koniec_godzina - poczatek_godzina) * 3;

            textboxy = new System.Windows.Forms.TextBox[ilosc_textboxow];
            for (int i = 0; i < textboxy.Length; i++)
            {
                textboxy[i] = new TextBox();
                textboxy[i].Location = new System.Drawing.Point(0, 0);
                textboxy[i].Name = "textbox" + i.ToString();
                textboxy[i].Size = new System.Drawing.Size(30, 20);
                textboxy[i].BackColor = System.Drawing.Color.White;
                textboxy[i].BorderStyle = BorderStyle.None;
                textboxy[i].TabIndex = i;
                textboxy[i].Enabled = false;
                textboxy[i].Text = poczatek_godzina + ":" + minuta + "0";
                

                licznik++; minuta += 2;
                if (minuta > 4) { minuta = 0; };
                if (licznik == 3 || licznik == 6 || licznik == 9 || licznik == 12 || licznik == 15
                || licznik == 18 || licznik == 21 || licznik == 24 || licznik == 27 || licznik == 30) { poczatek_godzina++; };
                flowLayoutPanel1.Controls.Add(textboxy[i]);
            }
            textboxy2 = new System.Windows.Forms.TextBox[ilosc_textboxow];
            for (int i = 0; i < textboxy2.Length; i++)
            {
                textboxy2[i] = new TextBox();
                textboxy2[i].Location = new System.Drawing.Point(0, 0);
                textboxy2[i].Name = "textboxy" + i.ToString();
                textboxy2[i].Size = new System.Drawing.Size(115, 20);
                textboxy2[i].Font = new System.Drawing.Font(textboxy2[i].Font, FontStyle.Bold);
                textboxy2[i].BackColor = System.Drawing.Color.LimeGreen;
                textboxy2[i].BorderStyle = BorderStyle.None;
                textboxy2[i].TabIndex = i;
                textboxy2[i].ReadOnly = true;
                flowLayoutPanel2.Controls.Add(textboxy2[i]);
            }
            textboxy3 = new System.Windows.Forms.TextBox[ilosc_textboxow];
            for (int i = 0; i < textboxy3.Length; i++)
            {
                textboxy3[i] = new TextBox();
                textboxy3[i].Location = new System.Drawing.Point(0, 0);
                textboxy3[i].Name = "textboxy3" + i.ToString();
                textboxy3[i].Size = new System.Drawing.Size(65, 20);
                textboxy3[i].BackColor = System.Drawing.Color.White;
                textboxy3[i].BorderStyle = BorderStyle.None;
                textboxy3[i].TabIndex = i;

                textboxy3[i].Enabled = false;
                flowLayoutPanel3.Controls.Add(textboxy3[i]);
            }
           

            MySqlConnection TInfo_wizyta = new MySqlConnection(Polaczenie_z_baza);

            string ZapytanieInfo_wizyta = "select * from przychodnia.pacjenci INNER JOIN przychodnia.info_wizyta ON przychodnia.pacjenci.Id_pacjenta=przychodnia.info_wizyta.Id_pacjenta  ;";
            MySqlCommand KwerendaPokazInfo_wizyte = new MySqlCommand(ZapytanieInfo_wizyta, TInfo_wizyta);
            MySqlDataReader CzytanieInfo_wizyta;

            try
            {
                TInfo_wizyta.Open();
                CzytanieInfo_wizyta = KwerendaPokazInfo_wizyte.ExecuteReader();
                while (CzytanieInfo_wizyta.Read())
                {
                    if (CzytanieInfo_wizyta.GetDateTime("Data_wizyty") == (Convert.ToDateTime(this.dateTimePicker1.Text)))
                    {
                        for (int i = 0; i < textboxy2.Length; i++)
                        {
                            if (CzytanieInfo_wizyta.GetString("Godzina_wizyty") == (textboxy[i].Text))
                            {
                                string imie = CzytanieInfo_wizyta.GetString("Imie");
                                string nazwisko = CzytanieInfo_wizyta.GetString("Nazwisko");
                                string Id = CzytanieInfo_wizyta.GetString("Id_pacjenta");
                              
                                textboxy2[i].Text = nazwisko + " " + imie;
                                textboxy2[i].BackColor = System.Drawing.Color.Salmon;
                                textboxy3[i].Text = Id;
                                textboxy2[i].Click += delegate
                                {
                                    pokaz_pacjenta(Id);
                                };
                                
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            dzisiejsze_wizyty();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                dateTimePicker2.Enabled = true;
                dateTimePicker3.Enabled = true;
                button3.Enabled = true;
            }
            if (checkBox2.Checked == false)
            {
                dateTimePicker2.Enabled = false;
                dateTimePicker3.Enabled = false;
                button3.Enabled = false;
            }
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                comboBox1.Enabled = true;
            }
            if (checkBox1.Checked == false)
            {
                button4.Enabled = false;
                comboBox1.Enabled = false; comboBox1.Text = "";
                comboBox2.Enabled = false; comboBox2.Text = "";
                comboBox3.Enabled = false; comboBox3.Text = "";
                comboBox4.Enabled = false; comboBox4.Text = "";
                comboBox5.Enabled = false; comboBox5.Text = "";
                numericUpDown1.Enabled = false; numericUpDown1.Value = 0;
                numericUpDown2.Enabled = false; numericUpDown2.Value = 0;
                numericUpDown3.Enabled = false; numericUpDown3.Value = 0;
                numericUpDown4.Enabled = false; numericUpDown4.Value = 0;
                numericUpDown5.Enabled = false; numericUpDown5.Value = 0;
                numericUpDown6.Enabled = false; numericUpDown6.Value = 0;
                numericUpDown7.Enabled = false; numericUpDown7.Value = 0;
                numericUpDown8.Enabled = false; numericUpDown8.Value = 0;
                numericUpDown9.Enabled = false; numericUpDown9.Value = 0;
                numericUpDown10.Enabled = false; numericUpDown10.Value = 0;

            }
            
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {

                button4.Enabled = false;
                comboBox2.Text = ""; comboBox3.Text = ""; comboBox4.Text = ""; comboBox5.Text = "";
                comboBox2.Enabled = false; comboBox3.Enabled = false; comboBox4.Enabled = false; comboBox5.Enabled = false;
                numericUpDown1.Value = 0; numericUpDown1.Enabled = false;
                numericUpDown2.Value = 0; numericUpDown2.Enabled = false;
                numericUpDown3.Value = 0; numericUpDown3.Enabled = false;
                numericUpDown4.Value = 0; numericUpDown4.Enabled = false;
                numericUpDown5.Value = 0; numericUpDown5.Enabled = false;
                numericUpDown6.Value = 0; numericUpDown6.Enabled = false;
                numericUpDown7.Value = 0; numericUpDown7.Enabled = false;
                numericUpDown8.Value = 0; numericUpDown8.Enabled = false;
                numericUpDown9.Value = 0; numericUpDown9.Enabled = false;
                numericUpDown10.Value = 0; numericUpDown10.Enabled = false;
            }
            if (comboBox1.Text != "")
            {
                button4.Enabled = true;
                comboBox2.Enabled = true;
                numericUpDown1.Enabled = true;
                numericUpDown10.Enabled = true;
            }
            MySqlConnection TLeki = new MySqlConnection(Polaczenie_z_baza);
            string ZapytanieLeki = "select * from przychodnia.lekarstwa where Nazwa_leku='" + comboBox1.Text + "';";
            MySqlCommand KwerendaPokazLeki = new MySqlCommand(ZapytanieLeki, TLeki);
            MySqlDataReader CzytanieLeki;

            try
            {
                TLeki.Open();
                CzytanieLeki = KwerendaPokazLeki.ExecuteReader();
                while (CzytanieLeki.Read())
                {
                    string Dawka_jednorazowo = CzytanieLeki.GetString("Dawka_jednorazowo");
                    string Liczba_dawek_na_dzien = CzytanieLeki.GetString("Liczba_dawek_na_dzien"); 
                    numericUpDown1.Value = Convert.ToDecimal(Dawka_jednorazowo);
                    numericUpDown10.Value = Convert.ToDecimal(Liczba_dawek_na_dzien);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text == "")
            {
                comboBox3.Text = ""; comboBox4.Text = ""; comboBox5.Text = "";
                comboBox3.Enabled = false; comboBox4.Enabled = false; comboBox5.Enabled = false;                
                numericUpDown2.Value = 0; numericUpDown2.Enabled = false;
                numericUpDown3.Value = 0; numericUpDown3.Enabled = false;
                numericUpDown4.Value = 0; numericUpDown4.Enabled = false;
                numericUpDown5.Value = 0; numericUpDown5.Enabled = false;
                numericUpDown6.Value = 0; numericUpDown6.Enabled = false;
                numericUpDown7.Value = 0; numericUpDown7.Enabled = false;
                numericUpDown8.Value = 0; numericUpDown8.Enabled = false;
                numericUpDown9.Value = 0; numericUpDown9.Enabled = false;
                            }
            if (comboBox2.Text != "")
            {
                comboBox3.Enabled = true;
                numericUpDown2.Enabled = true;
                numericUpDown9.Enabled = true;
            }
            MySqlConnection TLeki = new MySqlConnection(Polaczenie_z_baza);
            string ZapytanieLeki = "select * from przychodnia.lekarstwa where Nazwa_leku='" + comboBox2.Text + "';";
            MySqlCommand KwerendaPokazLeki = new MySqlCommand(ZapytanieLeki, TLeki);
            MySqlDataReader CzytanieLeki;

            try
            {
                TLeki.Open();
                CzytanieLeki = KwerendaPokazLeki.ExecuteReader();
                while (CzytanieLeki.Read())
                {
                    string Dawka_jednorazowo = CzytanieLeki.GetString("Dawka_jednorazowo");
                    string Liczba_dawek_na_dzien = CzytanieLeki.GetString("Liczba_dawek_na_dzien");
                    numericUpDown2.Value = Convert.ToDecimal(Dawka_jednorazowo);
                    numericUpDown9.Value = Convert.ToDecimal(Liczba_dawek_na_dzien);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.Text == "")
            {
                comboBox4.Text = ""; comboBox5.Text = "";
                comboBox4.Enabled = false; comboBox5.Enabled = false;
                numericUpDown3.Value = 0; numericUpDown3.Enabled = false;
                numericUpDown4.Value = 0; numericUpDown4.Enabled = false;
                numericUpDown5.Value = 0; numericUpDown5.Enabled = false;
                numericUpDown6.Value = 0; numericUpDown6.Enabled = false;
                numericUpDown7.Value = 0; numericUpDown7.Enabled = false;
                numericUpDown8.Value = 0; numericUpDown8.Enabled = false;
            }
            if (comboBox3.Text != "")
            {
                comboBox4.Enabled = true;
                numericUpDown3.Enabled = true;
                numericUpDown8.Enabled = true;
            }
            MySqlConnection TLeki = new MySqlConnection(Polaczenie_z_baza);
            string ZapytanieLeki = "select * from przychodnia.lekarstwa where Nazwa_leku='" + comboBox3.Text + "';";
            MySqlCommand KwerendaPokazLeki = new MySqlCommand(ZapytanieLeki, TLeki);
            MySqlDataReader CzytanieLeki;

            try
            {
                TLeki.Open();
                CzytanieLeki = KwerendaPokazLeki.ExecuteReader();
                while (CzytanieLeki.Read())
                {
                    string Dawka_jednorazowo = CzytanieLeki.GetString("Dawka_jednorazowo");
                    string Liczba_dawek_na_dzien = CzytanieLeki.GetString("Liczba_dawek_na_dzien");
                    numericUpDown3.Value = Convert.ToDecimal(Dawka_jednorazowo);
                    numericUpDown8.Value = Convert.ToDecimal(Liczba_dawek_na_dzien);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox4.Text == "")
            {
                comboBox5.Text = "";
                comboBox5.Enabled = false;
                numericUpDown4.Value = 0; numericUpDown4.Enabled = false;
                numericUpDown7.Value = 0; numericUpDown7.Enabled = false;
                numericUpDown5.Value = 0; numericUpDown5.Enabled = false;
                numericUpDown6.Value = 0; numericUpDown6.Enabled = false;
            }
            if (comboBox4.Text != "")
            {
                comboBox5.Enabled = true;
                numericUpDown4.Enabled = true;
                numericUpDown7.Enabled = true;
            }
            MySqlConnection TLeki = new MySqlConnection(Polaczenie_z_baza);
            string ZapytanieLeki = "select * from przychodnia.lekarstwa where Nazwa_leku='" + comboBox4.Text + "';";
            MySqlCommand KwerendaPokazLeki = new MySqlCommand(ZapytanieLeki, TLeki);
            MySqlDataReader CzytanieLeki;

            try
            {
                TLeki.Open();
                CzytanieLeki = KwerendaPokazLeki.ExecuteReader();
                while (CzytanieLeki.Read())
                {
                    string Dawka_jednorazowo = CzytanieLeki.GetString("Dawka_jednorazowo");
                    string Liczba_dawek_na_dzien = CzytanieLeki.GetString("Liczba_dawek_na_dzien");
                    numericUpDown4.Value = Convert.ToDecimal(Dawka_jednorazowo);
                    numericUpDown7.Value = Convert.ToDecimal(Liczba_dawek_na_dzien);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Logowanie FormaLogowanie = new Logowanie();
            FormaLogowanie.Show();
            MessageBox.Show("Pomyślnie wylogowano");

        }

        

        /// <summary>
        /// /////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        void load_table_lekarstwa()
        {
            string ZapytaniePokazBazeLekarstw = "select * from przychodnia.lekarstwa ;";

            MySqlConnection TLekarstwa = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeLekarstwa = new MySqlCommand(ZapytaniePokazBazeLekarstw, TLekarstwa);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazeLekarstwa;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            string Zapytanie = "select DISTINCT * from przychodnia.lekarstwa  ;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazSpecjalizacje = new MySqlCommand(Zapytanie, TLekarze);
            MySqlDataReader CzytanieLekarz;


            try
            {
                TLekarze.Open();
                CzytanieLekarz = KwerendaPokazSpecjalizacje.ExecuteReader();

                while (CzytanieLekarz.Read())
                {
                    string Nazwa = CzytanieLekarz.GetString("Nazwa_leku");
                    string Wskazania = CzytanieLekarz.GetString("Wskazania");
                    comboBox1.Items.Add(Nazwa);
                    comboBox2.Items.Add(Nazwa);
                    comboBox3.Items.Add(Nazwa);
                    comboBox4.Items.Add(Nazwa);
                    comboBox5.Items.Add(Nazwa);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void button11_Click(object sender, EventArgs e)
        {
            string ZapytanieDodajLekarstwa = "insert into przychodnia.lekarstwa (Nazwa_leku, Wskazania, Dawka_jednorazowo,Liczba_dawek_na_dzien) values('" + this.textBox4.Text + "','" + this.textBox5.Text + "','" + this.textBox6.Text + "','" + this.textBox3.Text + "') ;";

            MySqlConnection TLekarstwa = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajLekarstwa = new MySqlCommand(ZapytanieDodajLekarstwa, TLekarstwa);
            MySqlDataReader CzytanieLekarstwa;

            try
            {
                TLekarstwa.Open();
                CzytanieLekarstwa = KwerendaDodajLekarstwa.ExecuteReader();
                MessageBox.Show("Zapisano");

                while (CzytanieLekarstwa.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_lekarstwa();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            string ZapytaniePokazBazeLekarstwa = "select * from przychodnia.lekarstwa where Nazwa_leku like '" + this.textBox4.Text + "%" + "' ;";

            MySqlConnection TLekarstwa = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeLekarstwa = new MySqlCommand(ZapytaniePokazBazeLekarstwa, TLekarstwa);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazeLekarstwa;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            string ZapytanieDodajLekarstwa = "update przychodnia.lekarstwa set Id_lekarstwa='" + this.textBox7.Text + "',Nazwa_leku='" + this.textBox4.Text + "',Wskazania='" + this.textBox5.Text + "',Dawka_jednorazowo='" + this.textBox6.Text + "',Liczba_dawek_na_dzien='" + this.textBox3.Text + "' where Id_lekarstwa='" + this.textBox7.Text + "' ;";

            MySqlConnection TLekarstwa = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajLekarstwa = new MySqlCommand(ZapytanieDodajLekarstwa, TLekarstwa);
            MySqlDataReader CzytanieLekarstwa;

            try
            {
                TLekarstwa.Open();
                CzytanieLekarstwa = KwerendaDodajLekarstwa.ExecuteReader();
                MessageBox.Show("Zapisano");

                while (CzytanieLekarstwa.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_lekarstwa();
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            string ZapytanieDodajLekarstwa = "delete from przychodnia.lekarstwa where Id_lekarstwa='" + this.textBox7.Text + "';";

            MySqlConnection TLekarstwa = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajLekarstwa = new MySqlCommand(ZapytanieDodajLekarstwa, TLekarstwa);
            MySqlDataReader CzytanieLekarstwa;

            try
            {
                TLekarstwa.Open();
                CzytanieLekarstwa = KwerendaDodajLekarstwa.ExecuteReader();
                MessageBox.Show("Zapisano");

                while (CzytanieLekarstwa.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_lekarstwa();
        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int id;
            id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells["Id_lekarstwa"].Value.ToString());
            textBox7.Text = Convert.ToString(id);
            string ZapytaniePokazBazeLekarstwa = "select * from przychodnia.lekarstwa where Id_lekarstwa=" + id + "";

            MySqlConnection TLekarstwa = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeLekarstwa = new MySqlCommand(ZapytaniePokazBazeLekarstwa, TLekarstwa);
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(KwerendaPodazBazeLekarstwa);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                textBox4.Text = dr["Nazwa_leku"].ToString();
                textBox5.Text = dr["Wskazania"].ToString();
                textBox6.Text = dr["Dawka_jednorazowo"].ToString();
                textBox3.Text = dr["Liczba_dawek_na_dzien"].ToString();
            }
        }

        private void Modul_lekarz_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Czy napewno chcesz zamknąć aplikację?", "Zamykanie aplikacji", MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                {
                    Environment.Exit(0);
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string Zapytanie = "select * from przychodnia.lekarze where Id_lekarza='" + textBox2.Text + "';";
            string NazwiskoLek = " ";
            string ImieLek = " ";
            string Specj = " ";

            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokaz = new MySqlCommand(Zapytanie, TLekarze);
            MySqlDataReader CzytanieLekarz;

            try
            {
                TLekarze.Open();
                CzytanieLekarz = KwerendaPokaz.ExecuteReader();

                while (CzytanieLekarz.Read())
                {
                    NazwiskoLek = CzytanieLekarz.GetString("Nazwisko");
                    ImieLek = CzytanieLekarz.GetString("Imie");
                    Specj = CzytanieLekarz.GetString("Specjalizacja");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            string ZapytaniePacj = "select * from przychodnia.pacjenci where Id_pacjenta='" + label11.Text + "';";
            string NazwiskoPacj = " ";
            string ImiePacj = " ";
            string UlicaPacj = " ";
            string Nr_domu = " ";
            string Kod_poczt = " ";
            string Miasto = " ";
            string Pesel = " ";
            MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazPacjenta = new MySqlCommand(ZapytaniePacj, TPacjenci);
            MySqlDataReader CzytaniePacjent;

            try
            {
                TPacjenci.Open();
                CzytaniePacjent = KwerendaPokazPacjenta.ExecuteReader();

                while (CzytaniePacjent.Read())
                {
                    NazwiskoPacj = CzytaniePacjent.GetString("Nazwisko");
                    ImiePacj = CzytaniePacjent.GetString("Imie");
                    UlicaPacj = CzytaniePacjent.GetString("Ulica");
                    Nr_domu = CzytaniePacjent.GetString("Nr_domu");
                    Kod_poczt = CzytaniePacjent.GetString("Kod_pocztowy");
                    Miasto = CzytaniePacjent.GetString("Miasto");
                    Pesel = CzytaniePacjent.GetString("Pesel");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            FileStream gotowe = new FileStream("Recepta_gotowe.pdf", FileMode.Create);
            PdfReader pread = new PdfReader("Recepta_szablon.pdf");
            PdfStamper pstamp = new PdfStamper(pread, gotowe);

            PdfContentByte data_wystawienia = pstamp.GetOverContent(1); //WPISYWANIE DATE W RECEPTĘ
            data_wystawienia.BeginText();
            BaseFont data = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, false);
            data_wystawienia.SetFontAndSize(data, 12f);
            int lewo = PdfContentByte.ALIGN_LEFT;
            data_wystawienia.ShowTextAligned(lewo, dateTimePicker1.Text, 20, 25, 0);
            data_wystawienia.EndText();

            PdfContentByte dane_lekarskie = pstamp.GetOverContent(1);   //WPISYWANIE LEKARZA W RECPETĘ
            dane_lekarskie.BeginText();
            BaseFont dane_lekarz = BaseFont.CreateFont(BaseFont.TIMES_ITALIC, BaseFont.CP1250, false);
            dane_lekarskie.SetFontAndSize(dane_lekarz, 8f);
            dane_lekarskie.ShowTextAligned(lewo, ImieLek + " " + NazwiskoLek, 132, 30, 0);

            dane_lekarskie.ShowTextAligned(lewo, Specj, 142, 20, 0);
            dane_lekarskie.EndText();

            PdfContentByte dane_pacjenta = pstamp.GetOverContent(1);   //WPISYWANIE PACJENTA W RECPETĘ
            dane_pacjenta.BeginText();
            BaseFont dane_pacjent = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.CP1250, false);
            dane_pacjenta.SetFontAndSize(dane_pacjent, 10f);
            dane_pacjenta.ShowTextAligned(lewo, ImiePacj + " " + NazwiskoPacj, 35, 221, 0);
            dane_pacjenta.ShowTextAligned(lewo, UlicaPacj + " " + Nr_domu, 35, 210, 0);
            dane_pacjenta.ShowTextAligned(lewo, Kod_poczt + " " + Miasto, 35, 197, 0);
            dane_pacjenta.ShowTextAligned(lewo, "Pesel: " + Pesel, 35, 185, 0);
            dane_pacjenta.EndText();

            PdfContentByte leki = pstamp.GetOverContent(1);   //WPISYWANIE LEKÓW W RECPETĘ
            leki.BeginText();
            BaseFont lekarstwa = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
            dane_pacjenta.SetFontAndSize(lekarstwa, 9f);
          
            dane_pacjenta.ShowTextAligned(lewo, comboBox1.Text, 16, 152, 0); //lek1
            dane_pacjenta.ShowTextAligned(lewo, "Dawkowanie: " + numericUpDown1.Value + "x" + numericUpDown10.Value, 155, 152, 0);
            if (comboBox2.Text != "")
            {
                dane_pacjenta.ShowTextAligned(lewo, comboBox2.Text, 16, 131, 0); //lek2
                dane_pacjenta.ShowTextAligned(lewo, "Dawkowanie: " + numericUpDown2.Value + "x" + numericUpDown9.Value, 155, 131, 0);
            }
            if (comboBox3.Text != "")
            {
                dane_pacjenta.ShowTextAligned(lewo, comboBox3.Text, 16, 110, 0); //lek3
                dane_pacjenta.ShowTextAligned(lewo, "Dawkowanie: " + numericUpDown3.Value + "x" + numericUpDown8.Value, 155, 110, 0);
            }
            if (comboBox4.Text != "")
            {
                dane_pacjenta.ShowTextAligned(lewo, comboBox4.Text, 16, 89, 0); //lek4
                dane_pacjenta.ShowTextAligned(lewo, "Dawkowanie: " + numericUpDown4.Value + "x" + numericUpDown7.Value, 155, 89, 0);
            }
            if (comboBox5.Text != "")
            {
                dane_pacjenta.ShowTextAligned(lewo, comboBox5.Text, 16, 68, 0); //lek5
                dane_pacjenta.ShowTextAligned(lewo, "Dawkowanie: " + numericUpDown5.Value + "x" + numericUpDown6.Value, 155, 68, 0);
            }
            dane_pacjenta.EndText();



            pstamp.Close();
            Process process = new Process();
            process.StartInfo.FileName = "Recepta_gotowe.pdf";
            process.Start();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            FileStream gotowe = new FileStream("Rachunek_gotowe.pdf", FileMode.Create);
            PdfReader pread = new PdfReader("Rachunek.pdf");
            PdfStamper pstamp = new PdfStamper(pread, gotowe);

            PdfContentByte dane_lekarskie = pstamp.GetOverContent(1);   //WYPISYWANIE RACHUNKU
            dane_lekarskie.BeginText();
            int lewo = PdfContentByte.ALIGN_LEFT;
            BaseFont dane_lekarz = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
            dane_lekarskie.SetFontAndSize(dane_lekarz, 10f);
            dane_lekarskie.ShowTextAligned(lewo, dateTimePicker1.Text, 117, 67, 0);
            dane_lekarskie.ShowTextAligned(lewo, comboBox6.Text, 117, 122, 0);
            dane_lekarskie.EndText();

            pstamp.Close();


            Process process = new Process();
            process.StartInfo.FileName = "Rachunek_gotowe.pdf";
            process.Start();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if ((dateTimePicker2.Value <= dateTimePicker3.Value) && ( dateTimePicker2.Value >= DateTime.Today))
            {
                string Zapytanie = "select * from przychodnia.lekarze where Id_lekarza='" + textBox2.Text + "';";
                string NazwiskoLek = " ";
                string ImieLek = " ";


                MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand KwerendaPokaz = new MySqlCommand(Zapytanie, TLekarze);
                MySqlDataReader CzytanieLekarz;

                try
                {
                    TLekarze.Open();
                    CzytanieLekarz = KwerendaPokaz.ExecuteReader();

                    while (CzytanieLekarz.Read())
                    {
                        NazwiskoLek = CzytanieLekarz.GetString("Nazwisko");
                        ImieLek = CzytanieLekarz.GetString("Imie");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                string ZapytaniePacj = "select * from przychodnia.pacjenci where Id_pacjenta='" + label11.Text + "';";
                string NazwiskoPacj = " ";
                string ImiePacj = " ";
                string Data_urodzenia = " ";
                string Pesel = " ";
                MySqlConnection TPacjenci = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand KwerendaPokazPacjenta = new MySqlCommand(ZapytaniePacj, TPacjenci);
                MySqlDataReader CzytaniePacjent;

                try
                {
                    TPacjenci.Open();
                    CzytaniePacjent = KwerendaPokazPacjenta.ExecuteReader();

                    while (CzytaniePacjent.Read())
                    {
                        NazwiskoPacj = CzytaniePacjent.GetString("Nazwisko");
                        ImiePacj = CzytaniePacjent.GetString("Imie");
                        Data_urodzenia = CzytaniePacjent.GetString("Data_urodzenia");
                        dateTimePicker4.Visible = true;
                        dateTimePicker4.Text = Data_urodzenia;
                        Data_urodzenia = dateTimePicker4.Text;
                        dateTimePicker4.Visible = false;

                        Pesel = CzytaniePacjent.GetString("Pesel");

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }



                FileStream gotowe = new FileStream("zaswiadczenie_lek_gotowe.pdf", FileMode.Create);
                PdfReader pread = new PdfReader("zaswiadczenie_lek.pdf");
                PdfStamper pstamp = new PdfStamper(pread, gotowe);

                PdfContentByte data_wystawienia = pstamp.GetOverContent(1); //WPISYWANIE DATE 
                data_wystawienia.BeginText();                               //I OKRES ZWOLNIENIA W RECEPTĘ + ROZPOZNANIE
                BaseFont data = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, false);
                data_wystawienia.SetFontAndSize(data, 10f);
                int lewo = PdfContentByte.ALIGN_LEFT;
                data_wystawienia.ShowTextAligned(lewo, "Legnica", 260, 271, 0);
                data_wystawienia.ShowTextAligned(lewo, dateTimePicker1.Text, 340, 271, 0);
                data_wystawienia.ShowTextAligned(lewo, dateTimePicker2.Text, 100, 65, 0);
                data_wystawienia.ShowTextAligned(lewo, dateTimePicker3.Text, 220, 65, 0);
                data_wystawienia.ShowTextAligned(lewo, textBox1.Text, 100, 92, 0);
                data_wystawienia.EndText();

                PdfContentByte dane_lekarskie = pstamp.GetOverContent(1);   //WPISYWANIE LEKARZA W ZAŚWIADCZENIE
                dane_lekarskie.BeginText();
                BaseFont dane_lekarz = BaseFont.CreateFont(BaseFont.TIMES_ITALIC, BaseFont.CP1250, false);
                dane_lekarskie.SetFontAndSize(dane_lekarz, 8f);
                dane_lekarskie.ShowTextAligned(lewo, ImieLek + " " + NazwiskoLek, 293, 24, 0);
                dane_lekarskie.EndText();

                PdfContentByte dane_pacjenta = pstamp.GetOverContent(1);   //WPISYWANIE PACJENTA W ZAŚWIADCZENIE
                dane_pacjenta.BeginText();
                BaseFont dane_pacjent = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.CP1250, false);
                dane_pacjenta.SetFontAndSize(dane_pacjent, 12f);
                dane_pacjenta.ShowTextAligned(lewo, ImiePacj + " " + NazwiskoPacj, 98, 146, 0);
                dane_pacjenta.ShowTextAligned(lewo, Data_urodzenia, 98, 118, 0);

                dane_pacjenta.ShowTextAligned(lewo, Pesel, 265, 119, 0);
                dane_pacjenta.EndText();

                pstamp.Close();


                Process process = new Process();
                process.StartInfo.FileName = "zaswiadczenie_lek_gotowe.pdf";
                process.Start();
            }
            else
            {
                MessageBox.Show("Wprowadź poprawną datę");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            dateTimePicker5.Value = DateTime.Now;
            string ZapytanieDodajLekarstwa = "insert into przychodnia.wizyta (Data_wizyty, Id_lekarza, Id_pacjenta, Cel_wizyty, Objawy, Rozpoznanie) values('" + this.dateTimePicker5.Text + "','" + this.textBox2.Text + "','" + this.label11.Text + "','" + this.comboBox6.Text + "','" + richTextBox1.Text + "','" + textBox1.Text + "') ;";

            MySqlConnection TLekarstwa = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajLekarstwa = new MySqlCommand(ZapytanieDodajLekarstwa, TLekarstwa);
            MySqlDataReader CzytanieLekarstwa;

            try
            {
                TLekarstwa.Open();
                CzytanieLekarstwa = KwerendaDodajLekarstwa.ExecuteReader();
                MessageBox.Show("Zapisano");

                while (CzytanieLekarstwa.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Karta_pacjenta();
        }

        void Karta_pacjenta()
        {
            listView2.Items.Clear();
            listView2.View = View.Details;
            MySqlConnection TWizyty = new MySqlConnection(Polaczenie_z_baza);
            string ZapytanieWizyty = "select Data_wizyty, Cel_wizyty, Objawy, Rozpoznanie from przychodnia.wizyta where Id_pacjenta='" + label11.Text + "' ;";
            MySqlCommand KwerendaPokazWizyty = new MySqlCommand(ZapytanieWizyty, TWizyty);
            MySqlDataAdapter sda = new MySqlDataAdapter();
            sda.SelectCommand = KwerendaPokazWizyty;
            DataTable dt = new DataTable();
            sda.Fill(dt);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dane = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dane["Data_wizyty"].ToString());
                listitem.SubItems.Add(dane["Cel_wizyty"].ToString());
                listitem.SubItems.Add(dane["Objawy"].ToString());
                listitem.SubItems.Add(dane["Rozpoznanie"].ToString());
                listView2.Items.Add(listitem);
            }

        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox5.Text == "")
            {
                numericUpDown5.Value = 0; numericUpDown5.Enabled = false;
                numericUpDown6.Value = 0; numericUpDown6.Enabled = false;
            }
            if (comboBox5.Text != "")
            {
                numericUpDown5.Enabled = true;
                numericUpDown6.Enabled = true;
            }
            MySqlConnection TLeki = new MySqlConnection(Polaczenie_z_baza);
            string ZapytanieLeki = "select * from przychodnia.lekarstwa where Nazwa_leku='" + comboBox5.Text + "';";
            MySqlCommand KwerendaPokazLeki = new MySqlCommand(ZapytanieLeki, TLeki);
            MySqlDataReader CzytanieLeki;

            try
            {
                TLeki.Open();
                CzytanieLeki = KwerendaPokazLeki.ExecuteReader();
                while (CzytanieLeki.Read())
                {
                    string Dawka_jednorazowo = CzytanieLeki.GetString("Dawka_jednorazowo");
                    string Liczba_dawek_na_dzien = CzytanieLeki.GetString("Liczba_dawek_na_dzien");
                    numericUpDown5.Value = Convert.ToDecimal(Dawka_jednorazowo);
                    numericUpDown6.Value = Convert.ToDecimal(Liczba_dawek_na_dzien);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
    

       
}
