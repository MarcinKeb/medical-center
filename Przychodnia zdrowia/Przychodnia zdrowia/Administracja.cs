﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Przychodnia_zdrowia
{
    public partial class Administracja : Form
    {
        string plec = " ";
        string Polaczenie_z_baza = "datasource=localhost;port=3306;username=root;password=;Convert Zero Datetime=True;charset=utf8";
        public Administracja()
        {
            InitializeComponent();
            load_table_lekarze();
            load_table_recepcja();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            this.Hide();
            Logowanie FormaLogowanie = new Logowanie();
            FormaLogowanie.Show();
            MessageBox.Show("Pomyślnie wylogowano");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }

        private void PrzyciskDodajLekarza_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                plec = radioButton1.Text;
                
            }
            if (radioButton2.Checked)
            {
                plec = radioButton2.Text;
                
            }

            string ZapytanieDodajLekarza = "insert into przychodnia.lekarze (Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Specjalizacja,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon) values('" + this.OknoNazwLek.Text + "','" + this.OknoImieLek.Text + "','" + this.maskedTextBox5.Text + "','" + this.OknoMiejUrodzLek.Text + "','" + plec + "','" + this.maskedTextBox6.Text + "','" + this.OknoSpecjalizacjaLek.Text + "','" + this.OknoMiastoLek.Text + "','" + this.maskedTextBox7.Text + "','" + this.OknoUlicaLek.Text + "','" + this.OknoNrDomuLek.Text + "','" + this.maskedTextBox8.Text + "') ;";

            MySqlConnection TLekarz = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajLekarza = new MySqlCommand(ZapytanieDodajLekarza, TLekarz);
            MySqlDataReader CzytanieLekarz;

            try
            {
                TLekarz.Open();
                CzytanieLekarz = KwerendaDodajLekarza.ExecuteReader();
                MessageBox.Show("Zapisano");

                while (CzytanieLekarz.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_lekarze();
        }
        void load_table_lekarze()
        {
            string ZapytaniePokazBazeLekarzy = "select Id_lekarza,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Specjalizacja,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.lekarze where Aktywny=1;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeLekarzy = new MySqlCommand(ZapytaniePokazBazeLekarzy, TLekarze);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazeLekarzy;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //POKAZ - TABELE RECEPCJA//
        void load_table_recepcja()
        {
            string ZapytaniePokazBazeRecepcji = "select Id_recepcjonisty,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Stan_cywilny,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.recepcja where Aktywny=1;";
            MySqlConnection TRecepcja = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeRecepcji = new MySqlCommand(ZapytaniePokazBazeRecepcji, TRecepcja);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazeRecepcji;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView2.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void czysc_lekarz()
        {
            this.OknoNazwLek.Clear();
            this.OknoImieLek.Clear();
            this.maskedTextBox5.Clear();
            this.OknoMiejUrodzLek.Clear();
            pictureBox3.Visible = false;
            radioButton2.Checked = false;
            pictureBox1.Visible = false;
            radioButton1.Checked = false;
            this.maskedTextBox6.Clear();
            this.OknoSpecjalizacjaLek.Clear();
            this.OknoMiastoLek.Clear();
            this.maskedTextBox7.Clear();
            this.OknoUlicaLek.Clear();
            this.OknoNrDomuLek.Clear();
            this.maskedTextBox8.Clear();
            
        }

        void czysc_recepcjonista()
        {
            this.OknoNazwRec.Clear();
            this.OknoImieRec.Clear();
            this.maskedTextBox9.Clear();
            this.OknoMiejUrRec.Clear();
            radioButton3.Checked = false;
            pictureBox2.Visible = false;
            radioButton4.Checked = false;
            pictureBox4.Visible = false;
            this.maskedTextBox10.Clear();
            this.OknoStanCywilRec.Clear();
            this.OknoMiastoRec.Clear();
            this.maskedTextBox11.Clear();
            this.OknoUlicaRec.Clear();
            this.OknoNrDomuRec.Clear();
            this.maskedTextBox12.Clear();
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                plec = radioButton1.Text;             
            }
            if (radioButton2.Checked)
            {
                plec = radioButton2.Text;
            }

            string ZapytanieDodajLekarza = "update przychodnia.lekarze set Id_lekarza='" + this.Id_lekarza.Text + "',Nazwisko='" + this.OknoNazwLek.Text + "',Imie='" + this.OknoImieLek.Text + "',Data_urodzenia='" + this.maskedTextBox5.Text + "',Miejsce_urodzenia='" + this.OknoMiejUrodzLek.Text + "',Plec='" + plec + "',Pesel='" + this.maskedTextBox6.Text + "',Specjalizacja='" + this.OknoSpecjalizacjaLek.Text + "',Miasto='" + this.OknoMiastoLek.Text + "',Kod_pocztowy='" + this.maskedTextBox7.Text + "',Ulica='" + this.OknoUlicaLek.Text + "',Nr_domu='" + this.OknoNrDomuLek.Text + "',Telefon='" + this.maskedTextBox8.Text + "' where Id_lekarza='" + this.Id_lekarza.Text + "' ;";
            MySqlConnection TLekarz = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajLekarza = new MySqlCommand(ZapytanieDodajLekarza, TLekarz);
            MySqlDataReader CzytanieLekarz;

            try
            {
                TLekarz.Open();
                CzytanieLekarz = KwerendaDodajLekarza.ExecuteReader();
                MessageBox.Show("Zaktualizowano");
                while (CzytanieLekarz.Read())
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_lekarze();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            czysc_lekarz();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                pictureBox3.Visible = false;
                pictureBox1.Visible = true;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                pictureBox1.Visible = false;
                pictureBox3.Visible = true;
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;
            id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells["Id_lekarza"].Value.ToString());
            Id_lekarza.Text = Convert.ToString(id);
            string ZapytaniePokazBazeLekarzy = "select Id_lekarza,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Specjalizacja,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.lekarze where Aktywny=1 and Id_lekarza=" + id + "";

            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeLekarzy = new MySqlCommand(ZapytaniePokazBazeLekarzy, TLekarze);
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(KwerendaPodazBazeLekarzy);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                OknoNazwLek.Text = dr["Nazwisko"].ToString();
                OknoImieLek.Text = dr["Imie"].ToString();
                maskedTextBox5.Text = dr["Data_urodzenia"].ToString();
                OknoMiejUrodzLek.Text = dr["Miejsce_urodzenia"].ToString();
                maskedTextBox6.Text = dr["Pesel"].ToString();
                OknoMiastoLek.Text = dr["Miasto"].ToString();
                maskedTextBox7.Text = dr["Kod_pocztowy"].ToString();
                OknoUlicaLek.Text = dr["Ulica"].ToString();
                OknoNrDomuLek.Text = dr["Nr_domu"].ToString();
                maskedTextBox8.Text = dr["Telefon"].ToString();
                if ((dr["Plec"].ToString()) == "Mezczyzna") { radioButton1.Checked = true; };
                if ((dr["Plec"].ToString()) == "Kobieta") { radioButton2.Checked = true; };
                OknoSpecjalizacjaLek.Text = dr["Specjalizacja"].ToString();
            }
        }

        private void PrzyciskDodajRecepcjoniste_Click(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                plec = radioButton3.Text;
            }
            if (radioButton4.Checked)
            {
                plec = radioButton4.Text;
            }

            string ZapytanieDodajRecepcjoniste = "insert into przychodnia.recepcja (Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Stan_cywilny,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon) values('" + this.OknoNazwRec.Text + "','" + this.OknoImieRec.Text + "','" + this.maskedTextBox9.Text + "','" + this.OknoMiastoRec.Text + "','" + plec + "','" + this.maskedTextBox10.Text + "','" + this.OknoStanCywilRec + "','" + this.OknoMiastoRec.Text + "','" + this.maskedTextBox11.Text + "','" + this.OknoUlicaRec.Text + "','" + this.OknoNrDomuRec.Text + "','" + this.maskedTextBox12.Text + "') ;";
            MySqlConnection TRecepcja = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajRecepcjoniste = new MySqlCommand(ZapytanieDodajRecepcjoniste, TRecepcja);
            MySqlDataReader CzytanieRecepcja;

            try
            {
                TRecepcja.Open();
                CzytanieRecepcja = KwerendaDodajRecepcjoniste.ExecuteReader();
                MessageBox.Show("Zapisano");

                while (CzytanieRecepcja.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_recepcja();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                plec = radioButton3.Text;
                
            }
            if (radioButton4.Checked)
            {
                plec = radioButton4.Text;                
            }

            string ZapytanieDodajRecepcjoniste = "update przychodnia.recepcja set Id_recepcjonisty='" + this.Id_recepcjonisty.Text + "',Nazwisko='" + this.OknoNazwRec.Text + "',Imie='" + this.OknoImieRec.Text + "',Data_urodzenia='" + this.maskedTextBox9.Text + "',Miejsce_urodzenia='" + this.OknoMiejUrRec.Text + "',Plec='" + plec + "',Stan_cywilny='" + this.OknoStanCywilRec.Text + "',Pesel='" + this.maskedTextBox10.Text + "',Miasto='" + this.OknoMiastoRec.Text + "',Kod_pocztowy='" + this.maskedTextBox11.Text + "',Ulica='" + this.OknoUlicaRec.Text + "',Nr_domu='" + this.OknoNrDomuRec.Text + "',Telefon='" + this.maskedTextBox12.Text + "' where Id_recepcjonisty='" + this.Id_recepcjonisty.Text + "' ;";
            MySqlConnection TRecepcja = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajRecepcjoniste = new MySqlCommand(ZapytanieDodajRecepcjoniste, TRecepcja);
            MySqlDataReader CzytanieRecepcjonista;

            try
            {
                TRecepcja.Open();
                CzytanieRecepcjonista = KwerendaDodajRecepcjoniste.ExecuteReader();
                MessageBox.Show("Zaktualizowano");
                while (CzytanieRecepcjonista.Read())
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_recepcja();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            czysc_recepcjonista();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;
            id = Convert.ToInt32(dataGridView2.Rows[e.RowIndex].Cells["Id_recepcjonisty"].Value.ToString());
            Id_recepcjonisty.Text = Convert.ToString(id);
            string ZapytaniePokazBazeRecepcji = "select Id_recepcjonisty,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Stan_cywilny,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.recepcja where Aktywny=1 and Id_recepcjonisty=" + id + "";

            MySqlConnection TRecepcja = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeRecepcjonistow = new MySqlCommand(ZapytaniePokazBazeRecepcji, TRecepcja);
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(KwerendaPodazBazeRecepcjonistow);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                OknoNazwRec.Text = dr["Nazwisko"].ToString();
                OknoImieRec.Text = dr["Imie"].ToString();
                maskedTextBox9.Text = dr["Data_urodzenia"].ToString();
                OknoMiejUrRec.Text = dr["Miejsce_urodzenia"].ToString();
                maskedTextBox10.Text = dr["Pesel"].ToString();
                OknoMiastoRec.Text = dr["Miasto"].ToString();
                maskedTextBox11.Text = dr["Kod_pocztowy"].ToString();
                OknoUlicaRec.Text = dr["Ulica"].ToString();
                OknoNrDomuRec.Text = dr["Nr_domu"].ToString();
                maskedTextBox12.Text = dr["Telefon"].ToString();
                if ((dr["Plec"].ToString()) == "Mezczyzna") { radioButton3.Checked = true; };
                if ((dr["Plec"].ToString()) == "Kobieta") { radioButton4.Checked = true; };
                OknoStanCywilRec.Text = dr["Stan_cywilny"].ToString();
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                pictureBox2.Visible = true;
                pictureBox4.Visible = false;
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
            {
                pictureBox2.Visible = false;
                pictureBox4.Visible = true;
            }
        }

        private void OknoNazwLek_TextChanged(object sender, EventArgs e)
        {
            string ZapytaniePokazBazeLekarzy = "select Id_lekarza,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Specjalizacja,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.lekarze where Aktywny=1 and Nazwisko like '" + this.OknoNazwLek.Text + "%" + "' ;";

            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazePacjenci = new MySqlCommand(ZapytaniePokazBazeLekarzy, TLekarze);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazePacjenci;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OknoNazwRec_TextChanged(object sender, EventArgs e)
        {
            string ZapytaniePokazBazeRecepcji = "select Id_recepcjonisty,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Stan_cywilny,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.recepcja where Aktywny=1 and Nazwisko like '" + this.OknoNazwRec.Text + "%" + "' ;";

            MySqlConnection TRecepcja = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeRecepcji = new MySqlCommand(ZapytaniePokazBazeRecepcji, TRecepcja);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazeRecepcji;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView2.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                plec = radioButton3.Text;

            }
            if (radioButton4.Checked)
            {
                plec = radioButton4.Text;
            }

            string ZapytanieDodajRecepcjoniste = "update przychodnia.recepcja set Id_recepcjonisty='" + this.Id_recepcjonisty.Text + "',Nazwisko='" + this.OknoNazwRec.Text + "',Imie='" + this.OknoImieRec.Text + "',Data_urodzenia='" + this.maskedTextBox9.Text + "',Miejsce_urodzenia='" + this.OknoMiejUrRec.Text + "',Plec='" + plec + "',Stan_cywilny='" + this.OknoStanCywilRec.Text + "',Pesel='" + this.maskedTextBox10.Text + "',Miasto='" + this.OknoMiastoRec.Text + "',Kod_pocztowy='" + this.maskedTextBox11.Text + "',Ulica='" + this.OknoUlicaRec.Text + "',Nr_domu='" + this.OknoNrDomuRec.Text + "',Telefon='" + this.maskedTextBox12.Text + "',Aktywny='" + 0 + "' where Id_recepcjonisty='" + this.Id_recepcjonisty.Text + "' ;";
            MySqlConnection TRecepcja = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajRecepcjoniste = new MySqlCommand(ZapytanieDodajRecepcjoniste, TRecepcja);
            MySqlDataReader CzytanieRecepcjonista;

            try
            {
                TRecepcja.Open();
                CzytanieRecepcjonista = KwerendaDodajRecepcjoniste.ExecuteReader();
                MessageBox.Show("Usunięto");
                while (CzytanieRecepcjonista.Read())
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_recepcja();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                plec = radioButton1.Text;
            }
            if (radioButton2.Checked)
            {
                plec = radioButton2.Text;
            }

            string ZapytanieDodajLekarza = "update przychodnia.lekarze set Id_lekarza='" + this.Id_lekarza.Text + "',Nazwisko='" + this.OknoNazwLek.Text + "',Imie='" + this.OknoImieLek.Text + "',Data_urodzenia='" + this.maskedTextBox5.Text + "',Miejsce_urodzenia='" + this.OknoMiejUrodzLek.Text + "',Plec='" + plec + "',Pesel='" + this.maskedTextBox6.Text + "',Specjalizacja='" + this.OknoSpecjalizacjaLek.Text + "',Miasto='" + this.OknoMiastoLek.Text + "',Kod_pocztowy='" + this.maskedTextBox7.Text + "',Ulica='" + this.OknoUlicaLek.Text + "',Nr_domu='" + this.OknoNrDomuLek.Text + "',Telefon='" + this.maskedTextBox8.Text + "',Aktywny='" + 0 + "' where Id_lekarza='" + this.Id_lekarza.Text + "' ;";
            MySqlConnection TLekarz = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajLekarza = new MySqlCommand(ZapytanieDodajLekarza, TLekarz);
            MySqlDataReader CzytanieLekarz;

            try
            {
                TLekarz.Open();
                CzytanieLekarz = KwerendaDodajLekarza.ExecuteReader();
                MessageBox.Show("Usunięto");
                while (CzytanieLekarz.Read())
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_lekarze();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string ZapytaniePokazBazeLekarzy = "select Id_lekarza,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Specjalizacja,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.lekarze where Aktywny=0;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeLekarzy = new MySqlCommand(ZapytaniePokazBazeLekarzy, TLekarze);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazeLekarzy;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView3.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            string ZapytaniePokazBazeLekarzy = "select Id_recepcjonisty,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Stan_cywilny,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.recepcja where Aktywny=0;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeLekarzy = new MySqlCommand(ZapytaniePokazBazeLekarzy, TLekarze);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazeLekarzy;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView3.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            string ZapytaniePokazBazeLekarzy = "select Id_pacjenta,Nazwisko,Imie,Data_urodzenia,Miejsce_urodzenia,Plec,Pesel,Ubezpieczenie,Miasto,Kod_pocztowy,Ulica,Nr_domu,Telefon from przychodnia.pacjenci where Aktywny=0;";
            MySqlConnection TLekarze = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeLekarzy = new MySqlCommand(ZapytaniePokazBazeLekarzy, TLekarze);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazeLekarzy;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView3.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
