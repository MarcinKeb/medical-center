﻿namespace Przychodnia_zdrowia
{
    partial class Administracja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Administracja));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.Id_lekarza = new System.Windows.Forms.TextBox();
            this.maskedTextBox8 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox7 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox6 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox5 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.button12 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label24 = new System.Windows.Forms.Label();
            this.OknoMiejUrodzLek = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.OknoSpecjalizacjaLek = new System.Windows.Forms.TextBox();
            this.OknoNrDomuLek = new System.Windows.Forms.TextBox();
            this.OknoUlicaLek = new System.Windows.Forms.TextBox();
            this.OknoMiastoLek = new System.Windows.Forms.TextBox();
            this.OknoImieLek = new System.Windows.Forms.TextBox();
            this.OknoNazwLek = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.PrzyciskDodajLekarza = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.Id_recepcjonisty = new System.Windows.Forms.TextBox();
            this.maskedTextBox12 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox11 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox10 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox9 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label25 = new System.Windows.Forms.Label();
            this.OknoMiejUrRec = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.OknoStanCywilRec = new System.Windows.Forms.TextBox();
            this.OknoNrDomuRec = new System.Windows.Forms.TextBox();
            this.OknoUlicaRec = new System.Windows.Forms.TextBox();
            this.OknoMiastoRec = new System.Windows.Forms.TextBox();
            this.OknoImieRec = new System.Windows.Forms.TextBox();
            this.OknoNazwRec = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.PrzyciskDodajRecepcjoniste = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 91);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(769, 560);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.IndianRed;
            this.tabPage1.Controls.Add(this.pictureBox3);
            this.tabPage1.Controls.Add(this.Id_lekarza);
            this.tabPage1.Controls.Add(this.maskedTextBox8);
            this.tabPage1.Controls.Add(this.maskedTextBox7);
            this.tabPage1.Controls.Add(this.maskedTextBox6);
            this.tabPage1.Controls.Add(this.maskedTextBox5);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.button12);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.OknoMiejUrodzLek);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.OknoSpecjalizacjaLek);
            this.tabPage1.Controls.Add(this.OknoNrDomuLek);
            this.tabPage1.Controls.Add(this.OknoUlicaLek);
            this.tabPage1.Controls.Add(this.OknoMiastoLek);
            this.tabPage1.Controls.Add(this.OknoImieLek);
            this.tabPage1.Controls.Add(this.OknoNazwLek);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.PrzyciskDodajLekarza);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(761, 534);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Przychodnia_zdrowia.Properties.Resources.People_Doctorka_icon;
            this.pictureBox3.Location = new System.Drawing.Point(437, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(174, 239);
            this.pictureBox3.TabIndex = 123;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            // 
            // Id_lekarza
            // 
            this.Id_lekarza.Location = new System.Drawing.Point(349, 33);
            this.Id_lekarza.Name = "Id_lekarza";
            this.Id_lekarza.Size = new System.Drawing.Size(39, 20);
            this.Id_lekarza.TabIndex = 122;
            this.Id_lekarza.Visible = false;
            // 
            // maskedTextBox8
            // 
            this.maskedTextBox8.Location = new System.Drawing.Point(227, 325);
            this.maskedTextBox8.Mask = "000-000-000";
            this.maskedTextBox8.Name = "maskedTextBox8";
            this.maskedTextBox8.Size = new System.Drawing.Size(202, 20);
            this.maskedTextBox8.TabIndex = 112;
            // 
            // maskedTextBox7
            // 
            this.maskedTextBox7.Location = new System.Drawing.Point(10, 273);
            this.maskedTextBox7.Mask = "00-000";
            this.maskedTextBox7.Name = "maskedTextBox7";
            this.maskedTextBox7.Size = new System.Drawing.Size(202, 20);
            this.maskedTextBox7.TabIndex = 108;
            // 
            // maskedTextBox6
            // 
            this.maskedTextBox6.Location = new System.Drawing.Point(228, 169);
            this.maskedTextBox6.Mask = "00000000000";
            this.maskedTextBox6.Name = "maskedTextBox6";
            this.maskedTextBox6.Size = new System.Drawing.Size(202, 20);
            this.maskedTextBox6.TabIndex = 105;
            // 
            // maskedTextBox5
            // 
            this.maskedTextBox5.Location = new System.Drawing.Point(10, 116);
            this.maskedTextBox5.Mask = "0000-00-00";
            this.maskedTextBox5.Name = "maskedTextBox5";
            this.maskedTextBox5.Size = new System.Drawing.Size(202, 20);
            this.maskedTextBox5.TabIndex = 101;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.ForeColor = System.Drawing.Color.Gold;
            this.groupBox3.Location = new System.Drawing.Point(10, 155);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 37);
            this.groupBox3.TabIndex = 121;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Płeć";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(91, 17);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(61, 17);
            this.radioButton2.TabIndex = 39;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Kobieta";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(7, 18);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(78, 17);
            this.radioButton1.TabIndex = 38;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Mezczyzna";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // button12
            // 
            this.button12.Image = global::Przychodnia_zdrowia.Properties.Resources.Clear_icon;
            this.button12.Location = new System.Drawing.Point(438, 273);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(84, 74);
            this.button12.TabIndex = 117;
            this.button12.Text = "Wyczyść pola";
            this.button12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(6, 356);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Size = new System.Drawing.Size(744, 169);
            this.dataGridView1.TabIndex = 120;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.ForeColor = System.Drawing.SystemColors.Info;
            this.label24.Location = new System.Drawing.Point(128, 139);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(85, 13);
            this.label24.TabIndex = 119;
            this.label24.Text = "(RRRR-MM-DD)";
            // 
            // OknoMiejUrodzLek
            // 
            this.OknoMiejUrodzLek.Location = new System.Drawing.Point(228, 116);
            this.OknoMiejUrodzLek.Name = "OknoMiejUrodzLek";
            this.OknoMiejUrodzLek.Size = new System.Drawing.Size(203, 20);
            this.OknoMiejUrodzLek.TabIndex = 102;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label23.ForeColor = System.Drawing.Color.Gold;
            this.label23.Location = new System.Drawing.Point(225, 97);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(117, 16);
            this.label23.TabIndex = 115;
            this.label23.Text = "Miejsce urodzenia";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label21.ForeColor = System.Drawing.Color.Gold;
            this.label21.Location = new System.Drawing.Point(7, 97);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(99, 16);
            this.label21.TabIndex = 110;
            this.label21.Text = "Data urodzenia";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.White;
            this.button4.Image = global::Przychodnia_zdrowia.Properties.Resources.doctordelete;
            this.button4.Location = new System.Drawing.Point(617, 199);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(119, 93);
            this.button4.TabIndex = 116;
            this.button4.Text = "Przenieś do archiwum";
            this.button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Image = global::Przychodnia_zdrowia.Properties.Resources.doctorupdate;
            this.button2.Location = new System.Drawing.Point(617, 99);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(119, 93);
            this.button2.TabIndex = 114;
            this.button2.Text = "Zmień dane lekarza";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // OknoSpecjalizacjaLek
            // 
            this.OknoSpecjalizacjaLek.Location = new System.Drawing.Point(10, 222);
            this.OknoSpecjalizacjaLek.Name = "OknoSpecjalizacjaLek";
            this.OknoSpecjalizacjaLek.Size = new System.Drawing.Size(203, 20);
            this.OknoSpecjalizacjaLek.TabIndex = 106;
            // 
            // OknoNrDomuLek
            // 
            this.OknoNrDomuLek.Location = new System.Drawing.Point(10, 325);
            this.OknoNrDomuLek.Name = "OknoNrDomuLek";
            this.OknoNrDomuLek.Size = new System.Drawing.Size(203, 20);
            this.OknoNrDomuLek.TabIndex = 111;
            // 
            // OknoUlicaLek
            // 
            this.OknoUlicaLek.Location = new System.Drawing.Point(227, 273);
            this.OknoUlicaLek.Name = "OknoUlicaLek";
            this.OknoUlicaLek.Size = new System.Drawing.Size(203, 20);
            this.OknoUlicaLek.TabIndex = 109;
            // 
            // OknoMiastoLek
            // 
            this.OknoMiastoLek.Location = new System.Drawing.Point(227, 222);
            this.OknoMiastoLek.Name = "OknoMiastoLek";
            this.OknoMiastoLek.Size = new System.Drawing.Size(203, 20);
            this.OknoMiastoLek.TabIndex = 107;
            // 
            // OknoImieLek
            // 
            this.OknoImieLek.Location = new System.Drawing.Point(228, 65);
            this.OknoImieLek.Name = "OknoImieLek";
            this.OknoImieLek.Size = new System.Drawing.Size(203, 20);
            this.OknoImieLek.TabIndex = 99;
            // 
            // OknoNazwLek
            // 
            this.OknoNazwLek.Location = new System.Drawing.Point(10, 66);
            this.OknoNazwLek.Name = "OknoNazwLek";
            this.OknoNazwLek.Size = new System.Drawing.Size(203, 20);
            this.OknoNazwLek.TabIndex = 97;
            this.OknoNazwLek.TextChanged += new System.EventHandler(this.OknoNazwLek_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.ForeColor = System.Drawing.Color.Gold;
            this.label20.Location = new System.Drawing.Point(7, 203);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 16);
            this.label20.TabIndex = 104;
            this.label20.Text = "Specjalizacja";
            // 
            // PrzyciskDodajLekarza
            // 
            this.PrzyciskDodajLekarza.BackColor = System.Drawing.Color.White;
            this.PrzyciskDodajLekarza.Image = global::Przychodnia_zdrowia.Properties.Resources.doctoradd;
            this.PrzyciskDodajLekarza.Location = new System.Drawing.Point(617, 2);
            this.PrzyciskDodajLekarza.Name = "PrzyciskDodajLekarza";
            this.PrzyciskDodajLekarza.Size = new System.Drawing.Size(119, 93);
            this.PrzyciskDodajLekarza.TabIndex = 113;
            this.PrzyciskDodajLekarza.Text = "Dodaj lekarza";
            this.PrzyciskDodajLekarza.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.PrzyciskDodajLekarza.UseVisualStyleBackColor = false;
            this.PrzyciskDodajLekarza.Click += new System.EventHandler(this.PrzyciskDodajLekarza_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.White;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(6, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(148, 25);
            this.label11.TabIndex = 103;
            this.label11.Text = "Baza lekarzy";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.ForeColor = System.Drawing.Color.Gold;
            this.label12.Location = new System.Drawing.Point(227, 306);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 16);
            this.label12.TabIndex = 100;
            this.label12.Text = "Numer telefonu";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.ForeColor = System.Drawing.Color.Gold;
            this.label13.Location = new System.Drawing.Point(7, 306);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 16);
            this.label13.TabIndex = 98;
            this.label13.Text = "Numer domu";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.ForeColor = System.Drawing.Color.Gold;
            this.label14.Location = new System.Drawing.Point(225, 254);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 16);
            this.label14.TabIndex = 96;
            this.label14.Text = "Ulica";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.ForeColor = System.Drawing.Color.Gold;
            this.label15.Location = new System.Drawing.Point(7, 254);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 16);
            this.label15.TabIndex = 95;
            this.label15.Text = "Kod pocztowy";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.ForeColor = System.Drawing.Color.Gold;
            this.label16.Location = new System.Drawing.Point(225, 203);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 16);
            this.label16.TabIndex = 94;
            this.label16.Text = "Miasto";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.ForeColor = System.Drawing.Color.Gold;
            this.label17.Location = new System.Drawing.Point(225, 153);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 16);
            this.label17.TabIndex = 93;
            this.label17.Text = "Pesel";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.ForeColor = System.Drawing.Color.Gold;
            this.label18.Location = new System.Drawing.Point(225, 46);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 16);
            this.label18.TabIndex = 92;
            this.label18.Text = "Imię";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.ForeColor = System.Drawing.Color.Gold;
            this.label19.Location = new System.Drawing.Point(7, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 16);
            this.label19.TabIndex = 91;
            this.label19.Text = "Nazwisko";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Przychodnia_zdrowia.Properties.Resources.People_Doctor_icon;
            this.pictureBox1.Location = new System.Drawing.Point(437, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(174, 239);
            this.pictureBox1.TabIndex = 118;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.IndianRed;
            this.tabPage2.Controls.Add(this.pictureBox4);
            this.tabPage2.Controls.Add(this.Id_recepcjonisty);
            this.tabPage2.Controls.Add(this.maskedTextBox12);
            this.tabPage2.Controls.Add(this.maskedTextBox11);
            this.tabPage2.Controls.Add(this.maskedTextBox10);
            this.tabPage2.Controls.Add(this.maskedTextBox9);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.OknoMiejUrRec);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.label28);
            this.tabPage2.Controls.Add(this.OknoStanCywilRec);
            this.tabPage2.Controls.Add(this.OknoNrDomuRec);
            this.tabPage2.Controls.Add(this.OknoUlicaRec);
            this.tabPage2.Controls.Add(this.OknoMiastoRec);
            this.tabPage2.Controls.Add(this.OknoImieRec);
            this.tabPage2.Controls.Add(this.OknoNazwRec);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.button13);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.PrzyciskDodajRecepcjoniste);
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(761, 534);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Przychodnia_zdrowia.Properties.Resources.Recepcjonistka;
            this.pictureBox4.Location = new System.Drawing.Point(437, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(174, 172);
            this.pictureBox4.TabIndex = 125;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Visible = false;
            // 
            // Id_recepcjonisty
            // 
            this.Id_recepcjonisty.Location = new System.Drawing.Point(343, 34);
            this.Id_recepcjonisty.Name = "Id_recepcjonisty";
            this.Id_recepcjonisty.Size = new System.Drawing.Size(38, 20);
            this.Id_recepcjonisty.TabIndex = 124;
            this.Id_recepcjonisty.Visible = false;
            // 
            // maskedTextBox12
            // 
            this.maskedTextBox12.Location = new System.Drawing.Point(228, 325);
            this.maskedTextBox12.Mask = "000-000-000";
            this.maskedTextBox12.Name = "maskedTextBox12";
            this.maskedTextBox12.Size = new System.Drawing.Size(203, 20);
            this.maskedTextBox12.TabIndex = 104;
            // 
            // maskedTextBox11
            // 
            this.maskedTextBox11.Location = new System.Drawing.Point(10, 273);
            this.maskedTextBox11.Mask = "00-000";
            this.maskedTextBox11.Name = "maskedTextBox11";
            this.maskedTextBox11.Size = new System.Drawing.Size(203, 20);
            this.maskedTextBox11.TabIndex = 101;
            // 
            // maskedTextBox10
            // 
            this.maskedTextBox10.Location = new System.Drawing.Point(228, 169);
            this.maskedTextBox10.Mask = "00000000000";
            this.maskedTextBox10.Name = "maskedTextBox10";
            this.maskedTextBox10.Size = new System.Drawing.Size(203, 20);
            this.maskedTextBox10.TabIndex = 98;
            // 
            // maskedTextBox9
            // 
            this.maskedTextBox9.Location = new System.Drawing.Point(10, 116);
            this.maskedTextBox9.Mask = "0000-00-00";
            this.maskedTextBox9.Name = "maskedTextBox9";
            this.maskedTextBox9.Size = new System.Drawing.Size(203, 20);
            this.maskedTextBox9.TabIndex = 96;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioButton4);
            this.groupBox5.Controls.Add(this.radioButton3);
            this.groupBox5.ForeColor = System.Drawing.Color.Gold;
            this.groupBox5.Location = new System.Drawing.Point(10, 155);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(203, 37);
            this.groupBox5.TabIndex = 123;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Płeć";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(91, 17);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(61, 17);
            this.radioButton4.TabIndex = 56;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Kobieta";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(7, 18);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(78, 17);
            this.radioButton3.TabIndex = 55;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Mezczyzna";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // dataGridView2
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.Location = new System.Drawing.Point(10, 356);
            this.dataGridView2.Name = "dataGridView2";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView2.Size = new System.Drawing.Size(740, 166);
            this.dataGridView2.TabIndex = 122;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label25.ForeColor = System.Drawing.SystemColors.Info;
            this.label25.Location = new System.Drawing.Point(128, 139);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(85, 13);
            this.label25.TabIndex = 120;
            this.label25.Text = "(RRRR-MM-DD)";
            // 
            // OknoMiejUrRec
            // 
            this.OknoMiejUrRec.Location = new System.Drawing.Point(228, 116);
            this.OknoMiejUrRec.Name = "OknoMiejUrRec";
            this.OknoMiejUrRec.Size = new System.Drawing.Size(203, 20);
            this.OknoMiejUrRec.TabIndex = 97;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label26.ForeColor = System.Drawing.Color.Gold;
            this.label26.Location = new System.Drawing.Point(225, 97);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(117, 16);
            this.label26.TabIndex = 119;
            this.label26.Text = "Miejsce urodzenia";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.ForeColor = System.Drawing.Color.Gold;
            this.label28.Location = new System.Drawing.Point(7, 97);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(99, 16);
            this.label28.TabIndex = 118;
            this.label28.Text = "Data urodzenia";
            // 
            // OknoStanCywilRec
            // 
            this.OknoStanCywilRec.Location = new System.Drawing.Point(10, 222);
            this.OknoStanCywilRec.Name = "OknoStanCywilRec";
            this.OknoStanCywilRec.Size = new System.Drawing.Size(203, 20);
            this.OknoStanCywilRec.TabIndex = 99;
            // 
            // OknoNrDomuRec
            // 
            this.OknoNrDomuRec.Location = new System.Drawing.Point(10, 325);
            this.OknoNrDomuRec.Name = "OknoNrDomuRec";
            this.OknoNrDomuRec.Size = new System.Drawing.Size(203, 20);
            this.OknoNrDomuRec.TabIndex = 103;
            // 
            // OknoUlicaRec
            // 
            this.OknoUlicaRec.Location = new System.Drawing.Point(228, 273);
            this.OknoUlicaRec.Name = "OknoUlicaRec";
            this.OknoUlicaRec.Size = new System.Drawing.Size(203, 20);
            this.OknoUlicaRec.TabIndex = 102;
            // 
            // OknoMiastoRec
            // 
            this.OknoMiastoRec.Location = new System.Drawing.Point(228, 222);
            this.OknoMiastoRec.Name = "OknoMiastoRec";
            this.OknoMiastoRec.Size = new System.Drawing.Size(203, 20);
            this.OknoMiastoRec.TabIndex = 100;
            // 
            // OknoImieRec
            // 
            this.OknoImieRec.Location = new System.Drawing.Point(228, 65);
            this.OknoImieRec.Name = "OknoImieRec";
            this.OknoImieRec.Size = new System.Drawing.Size(203, 20);
            this.OknoImieRec.TabIndex = 95;
            // 
            // OknoNazwRec
            // 
            this.OknoNazwRec.Location = new System.Drawing.Point(10, 66);
            this.OknoNazwRec.Name = "OknoNazwRec";
            this.OknoNazwRec.Size = new System.Drawing.Size(203, 20);
            this.OknoNazwRec.TabIndex = 94;
            this.OknoNazwRec.TextChanged += new System.EventHandler(this.OknoNazwRec_TextChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label29.ForeColor = System.Drawing.Color.Gold;
            this.label29.Location = new System.Drawing.Point(7, 203);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(81, 16);
            this.label29.TabIndex = 117;
            this.label29.Text = "Stan cywilny";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label30.ForeColor = System.Drawing.Color.Gold;
            this.label30.Location = new System.Drawing.Point(227, 306);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(98, 16);
            this.label30.TabIndex = 116;
            this.label30.Text = "Numer telefonu";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label31.ForeColor = System.Drawing.Color.Gold;
            this.label31.Location = new System.Drawing.Point(7, 306);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(85, 16);
            this.label31.TabIndex = 115;
            this.label31.Text = "Numer domu";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label32.ForeColor = System.Drawing.Color.Gold;
            this.label32.Location = new System.Drawing.Point(225, 254);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(39, 16);
            this.label32.TabIndex = 114;
            this.label32.Text = "Ulica";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label33.ForeColor = System.Drawing.Color.Gold;
            this.label33.Location = new System.Drawing.Point(7, 254);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(91, 16);
            this.label33.TabIndex = 113;
            this.label33.Text = "Kod pocztowy";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label34.ForeColor = System.Drawing.Color.Gold;
            this.label34.Location = new System.Drawing.Point(225, 203);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(48, 16);
            this.label34.TabIndex = 111;
            this.label34.Text = "Miasto";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label35.ForeColor = System.Drawing.Color.Gold;
            this.label35.Location = new System.Drawing.Point(225, 153);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(43, 16);
            this.label35.TabIndex = 109;
            this.label35.Text = "Pesel";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label36.ForeColor = System.Drawing.Color.Gold;
            this.label36.Location = new System.Drawing.Point(225, 46);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(33, 16);
            this.label36.TabIndex = 107;
            this.label36.Text = "Imię";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label37.ForeColor = System.Drawing.Color.Gold;
            this.label37.Location = new System.Drawing.Point(7, 46);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(66, 16);
            this.label37.TabIndex = 106;
            this.label37.Text = "Nazwisko";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(6, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(155, 25);
            this.label10.TabIndex = 93;
            this.label10.Text = "Baza recepcji";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(466, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 16);
            this.label1.TabIndex = 92;
            // 
            // button13
            // 
            this.button13.Image = global::Przychodnia_zdrowia.Properties.Resources.Clear_icon;
            this.button13.Location = new System.Drawing.Point(437, 276);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(83, 74);
            this.button13.TabIndex = 112;
            this.button13.Text = "Wyczyść pola";
            this.button13.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.Image = global::Przychodnia_zdrowia.Properties.Resources.recepcjadelete;
            this.button5.Location = new System.Drawing.Point(617, 205);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(119, 93);
            this.button5.TabIndex = 110;
            this.button5.Text = "Przenieś do archiwum";
            this.button5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.Image = global::Przychodnia_zdrowia.Properties.Resources.recepcjaupdate;
            this.button3.Location = new System.Drawing.Point(617, 104);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 93);
            this.button3.TabIndex = 108;
            this.button3.Text = "Zmień dane ";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // PrzyciskDodajRecepcjoniste
            // 
            this.PrzyciskDodajRecepcjoniste.BackColor = System.Drawing.Color.White;
            this.PrzyciskDodajRecepcjoniste.Image = global::Przychodnia_zdrowia.Properties.Resources.recepcjaadd;
            this.PrzyciskDodajRecepcjoniste.Location = new System.Drawing.Point(617, 3);
            this.PrzyciskDodajRecepcjoniste.Name = "PrzyciskDodajRecepcjoniste";
            this.PrzyciskDodajRecepcjoniste.Size = new System.Drawing.Size(119, 93);
            this.PrzyciskDodajRecepcjoniste.TabIndex = 105;
            this.PrzyciskDodajRecepcjoniste.Text = "Dodaj recepcjoniste";
            this.PrzyciskDodajRecepcjoniste.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.PrzyciskDodajRecepcjoniste.UseVisualStyleBackColor = false;
            this.PrzyciskDodajRecepcjoniste.Click += new System.EventHandler(this.PrzyciskDodajRecepcjoniste_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Przychodnia_zdrowia.Properties.Resources.Recepcjonista;
            this.pictureBox2.Location = new System.Drawing.Point(437, -2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(174, 172);
            this.pictureBox2.TabIndex = 121;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.IndianRed;
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Controls.Add(this.button11);
            this.tabPage3.Controls.Add(this.button10);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(761, 534);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(17, 123);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(733, 315);
            this.dataGridView3.TabIndex = 4;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.DodgerBlue;
            this.button11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Image = global::Przychodnia_zdrowia.Properties.Resources.App_login_manager_icon;
            this.button11.Location = new System.Drawing.Point(243, 41);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(107, 76);
            this.button11.TabIndex = 3;
            this.button11.Text = "Baza pacjentów";
            this.button11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.DodgerBlue;
            this.button10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Image = global::Przychodnia_zdrowia.Properties.Resources.recepcjonistkaicon;
            this.button10.Location = new System.Drawing.Point(130, 41);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(107, 76);
            this.button10.TabIndex = 2;
            this.button10.Text = "Baza recepcji";
            this.button10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DodgerBlue;
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Image = global::Przychodnia_zdrowia.Properties.Resources.People_Doctor_Female_icon;
            this.button6.Location = new System.Drawing.Point(17, 41);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(107, 76);
            this.button6.TabIndex = 1;
            this.button6.Text = "Baza lekarzy";
            this.button6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(8, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Archiwum";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Przychodnia_zdrowia.Properties.Resources.logo_przychodnia;
            this.pictureBox5.Location = new System.Drawing.Point(293, 8);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(292, 85);
            this.pictureBox5.TabIndex = 28;
            this.pictureBox5.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Image = global::Przychodnia_zdrowia.Properties.Resources.drawer_archive_icon;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.Location = new System.Drawing.Point(196, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 81);
            this.button1.TabIndex = 27;
            this.button1.Text = "Archiwum";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button19.Image = global::Przychodnia_zdrowia.Properties.Resources.Log_Out_icon;
            this.button19.Location = new System.Drawing.Point(588, 12);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(90, 74);
            this.button19.TabIndex = 25;
            this.button19.Text = "Wyloguj";
            this.button19.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button9.Image = global::Przychodnia_zdrowia.Properties.Resources.Action_exit_icon1;
            this.button9.Location = new System.Drawing.Point(682, 12);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(82, 74);
            this.button9.TabIndex = 26;
            this.button9.Text = "Zamknij";
            this.button9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button8.Image = global::Przychodnia_zdrowia.Properties.Resources.recepcjonistkaicon;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button8.Location = new System.Drawing.Point(100, 5);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(90, 81);
            this.button8.TabIndex = 24;
            this.button8.Text = "Baza recepcji";
            this.button8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button7.Image = global::Przychodnia_zdrowia.Properties.Resources.People_Doctor_Female_icon;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button7.Location = new System.Drawing.Point(4, 5);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(90, 81);
            this.button7.TabIndex = 23;
            this.button7.Text = "Baza lekarzy";
            this.button7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 91);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(769, 21);
            this.panel1.TabIndex = 29;
            // 
            // Administracja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 657);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Administracja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administracja";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox Id_lekarza;
        private System.Windows.Forms.MaskedTextBox maskedTextBox8;
        private System.Windows.Forms.MaskedTextBox maskedTextBox7;
        private System.Windows.Forms.MaskedTextBox maskedTextBox6;
        private System.Windows.Forms.MaskedTextBox maskedTextBox5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox OknoMiejUrodzLek;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox OknoSpecjalizacjaLek;
        private System.Windows.Forms.TextBox OknoNrDomuLek;
        private System.Windows.Forms.TextBox OknoUlicaLek;
        private System.Windows.Forms.TextBox OknoMiastoLek;
        private System.Windows.Forms.TextBox OknoImieLek;
        private System.Windows.Forms.TextBox OknoNazwLek;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button PrzyciskDodajLekarza;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox Id_recepcjonisty;
        private System.Windows.Forms.MaskedTextBox maskedTextBox12;
        private System.Windows.Forms.MaskedTextBox maskedTextBox11;
        private System.Windows.Forms.MaskedTextBox maskedTextBox10;
        private System.Windows.Forms.MaskedTextBox maskedTextBox9;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox OknoMiejUrRec;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox OknoStanCywilRec;
        private System.Windows.Forms.TextBox OknoNrDomuRec;
        private System.Windows.Forms.TextBox OknoUlicaRec;
        private System.Windows.Forms.TextBox OknoMiastoRec;
        private System.Windows.Forms.TextBox OknoImieRec;
        private System.Windows.Forms.TextBox OknoNazwRec;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button PrzyciskDodajRecepcjoniste;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
    }
}